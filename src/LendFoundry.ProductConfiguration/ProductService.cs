﻿using System.Linq;
using System;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.TemplateManager;
using LendFoundry.Foundation.Date;
using LendFoundry.StatusManagement;
using LendFoundry.Foundation.Lookup;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using LendFoundry.EventHub;

namespace LendFoundry.ProductConfiguration
{
    public class ProductService : IProductService
    {
        public ProductService(IProductRepository productRepository,
            IEventHubClient eventHub,
            ITemplateManagerService templateManagerService,
            IProductGroupRepository productGroupRepository, ITenantTime tenantTime, IProductParametersRepository productParametersRepository, IEntityStatusService entityStatusService, ILookupService lookup, IProductTemplateRepository productTemplateRepository, IProductFeeParametersRepository productFeeParametersRepository)
        {
            if (productRepository == null) throw new ArgumentNullException(nameof(productRepository));
            if (eventHub == null) throw new ArgumentNullException(nameof(eventHub));

            ProductRepository = productRepository;
            EventHub = eventHub;
            TemplateManagerService = templateManagerService;
            TenantTime = tenantTime;
            ProductParametersRepository = productParametersRepository;
            ProductGroupRepository = productGroupRepository;
            EntityStatusService = entityStatusService;
            Lookup = lookup;
            ProductTemplateRepository = productTemplateRepository;
            ProductFeeParametersRepository = productFeeParametersRepository;
        }

        #region Variables

        private IProductFeeParametersRepository ProductFeeParametersRepository { get; set; }
        private ILookupService Lookup { get; }
        private IProductTemplateRepository ProductTemplateRepository { get; set; }
        private IProductParametersRepository ProductParametersRepository { get; set; }
        private IProductGroupRepository ProductGroupRepository { get; set; }
        private IProductRepository ProductRepository { get; }
        private IEntityStatusService EntityStatusService { get; }
        private IEventHubClient EventHub { get; }
        private ITemplateManagerService TemplateManagerService { get; }
        private ITenantTime TenantTime { get; }

        #endregion

        #region Get, Add, Update, Activate, Expire, Delete Product

        public async Task<List<IProduct>> GetAll()
        {
            var product = await ProductRepository.GetAll();
            if (product == null)
                throw new NotFoundException("Products not found");
            return product;
        }

        public async Task<IProduct> Get(string productId)
        {
            ProductValidator.ValidateProductId(productId);

            var product = await ProductRepository.GetProductDetails(productId);
            if (product == null)
                throw new NotFoundException("Product not found");
            return product;
        }
     
        public async Task<string> Add(IProductRequestModel productModel)
        {
            ProductValidator.ValidateProductOnAdd(productModel);

            var productDetail = await ProductRepository.GetProductDetails(productModel.ProductId.ToLower());
            if (productDetail != null)
                throw new NotFoundException("Product Already exsist with this productId");

            var product = productModel.ToProduct();
            var templateDetail = ProductTemplateRepository.GetProductTemplate(productModel.ProductType).Result;
            if (templateDetail == null)
                throw new InvalidOperationException($"Product Template details does not found for portfolioType {productModel.ProductType.ToString()}");

            List<IWorkFlow> workFlows = AddWorkFlowDetail(productModel, templateDetail);
            List<IConsentDocument> consents = await AddConsentDetail(productModel);
            product.Documents = consents;
            product.WorkFlow = workFlows;
            ProductRepository.Add(product);

            await AddTemplateDetails(product, templateDetail);
           

            await EventHub.Publish(new Events.ProductAdded()
            {
                ProductId = product.Id,
                Product = product
            });

            return product.Id;
        }

        private async Task AddTemplateDetails(IProduct product, IProductTemplate templateDetail)
        {
            JObject objContent = (JObject)JsonConvert.DeserializeObject(templateDetail.Template.ToString());
            var parameters = objContent["ParametersToBeAdd"];
            if (parameters != null)
            {
                var parameterList = JsonConvert.DeserializeObject<List<ParameterValue>>(parameters.ToString());

                foreach (var param in parameterList)
                {
                    var productGroupAlreadyExists = await ProductGroupRepository.GetById(product.ProductId, param.GroupName);
                    if (productGroupAlreadyExists == null)
                    {
                        var groupRequest = new ProductGroupRequest();
                        groupRequest.Name = param.GroupName;
                        groupRequest.GroupId = param.GroupName;
                        groupRequest.Description = param.Description;
                        groupRequest.Version = "1.0";

                        productGroupAlreadyExists = await AddProductGroup(product.ProductId, groupRequest);
                    }

                    var parameterRequest = new ProductParametersRequest();
                    parameterRequest.Name = param.Name;
                    parameterRequest.ParameterId = param.ParameterId;
                    parameterRequest.Value = param.Value;
                    parameterRequest.UnitValue = param.UnitValue;
                    parameterRequest.ParameterType = param.ParameterType;
                    parameterRequest.Description = param.Description;
                    parameterRequest.ApplicableEvents = param.ApplicableEvents;
                    parameterRequest.GroupId = productGroupAlreadyExists.GroupId;
                    parameterRequest.IsSystem = true;
                    await AddProductParameters(product.ProductId, parameterRequest);
                }
            }


            var feeParameters = objContent["FeeParametersToBeAdd"];
            if (feeParameters != null)
            {
                var parameterList = JsonConvert.DeserializeObject<List<FeeParameterValue>>(feeParameters.ToString());

                foreach (var param in parameterList)
                {
                    var productGroupAlreadyExists = await ProductGroupRepository.GetById(product.ProductId, param.GroupName);
                    if (productGroupAlreadyExists == null)
                    {
                        var groupRequest = new ProductGroupRequest();
                        groupRequest.Name = param.GroupName;
                        groupRequest.GroupId = param.GroupName;
                        groupRequest.Description = param.Description;
                        groupRequest.Version = "1.0";

                        productGroupAlreadyExists = await AddProductGroup(product.ProductId, groupRequest);
                    }

                    var parameterRequest = new ProductFeeParametersRequest();
                    parameterRequest.Name = param.Name;
                    parameterRequest.FeeId = param.FeeId;
                    parameterRequest.FeeType = param.FeeType;
                    parameterRequest.FeeAmount =Convert.ToDouble(param.FeeAmount);
                    parameterRequest.FrequencyType = param.FeePaymentFrequency;
                    parameterRequest.Description = param.Description;
                    parameterRequest.Events = param.ApplicableEvents;
                    parameterRequest.GroupId = productGroupAlreadyExists.GroupId;
                    parameterRequest.AppliedOnType = param.AppliedOnType;
                    parameterRequest.AutoPay = param.AutoPay;
                    parameterRequest.IsAccrue = param.IsAccrue;
                    parameterRequest.IsSystem = true;
                    await AddProductFeeParameters(product.ProductId, parameterRequest);
                }
            }
        }

        private List<IWorkFlow> AddWorkFlowDetail(IProductRequestModel productModel, IProductTemplate templateDetail)
        {

            JObject objContent = (JObject)JsonConvert.DeserializeObject(templateDetail.Template.ToString());

            List<IWorkFlow> workFlows = new List<IWorkFlow>();
            foreach (var item in productModel.WorkFlow)
            {
                ProductValidator.ValidateWorkFlow(item);
                ValidateWorkFlowType(objContent, item.WorkFlowType.ToString(), productModel.ProductType.ToString());
                ValidateEntityType(objContent, item.EntityType.ToString(), productModel.ProductType.ToString());

                var workFlow = new WorkFlow();
                workFlow.Id = GenerateUniqueId();
                workFlow.EntityType = item.EntityType;
                workFlow.Description = item.Description;
                workFlow.ValidFrom = new TimeBucket(item.ValidFrom);
                workFlow.ExpiresOn = new TimeBucket(item.ExpiresOn);
                workFlow.WorkFlowName = item.WorkFlowName;
                workFlow.WorkFlowType = item.WorkFlowType;
                workFlows.Add(workFlow);

            }

            return workFlows;
        }

        private void ValidateWorkFlowType(JObject objContent, string workFlowType, string productType)
        {
            var workFlowTypes = objContent["workFlowType"];
            var workFlows = JsonConvert.DeserializeObject<List<string>>(workFlowTypes.ToString());
            if (!workFlows.Any(x => x.ToString().ToLower() == workFlowType.ToLower()))
                throw new InvalidOperationException($"WorkFlow type does not match with the product-template configuration for workFlowType {workFlowType} and productType {productType}");

        }

        private void ValidateEntityType(JObject objContent, string entityType, string productType)
        {
            var SupportedEntities = objContent["SupportedEntities"];
            var entityTypes = JsonConvert.DeserializeObject<List<string>>(SupportedEntities.ToString());
            if (!entityTypes.Any(x => x.ToString().ToLower() == entityType.ToString().ToLower()))
                throw new InvalidOperationException($"EntityType does not match with the product-template configuration for entityType {SupportedEntities} and productType {productType}");

        }

        private async Task<List<IConsentDocument>> AddConsentDetail(IProductRequestModel productModel)
        {
            List<IConsentDocument> consents = new List<IConsentDocument>();
            foreach (var item in productModel.Documents)
            {
                ProductValidator.ValidateConsentDocument(item);

                ITemplate templateDetails = null;

                ITemplate template = new Template();
                template.Name = item.ConsentName;
                template.Format = item.Format;
                template.Body = item.Body;
                template.Properties = item.Properties;
                template.Title = item.Title;
                template.Version = item.Version;
                templateDetails = await TemplateManagerService.Add(template);

                IConsentDocument consentDocumentObj = new ConsentDocument();
                consentDocumentObj.ConsentCategory = item.ConsentCategory;
                consentDocumentObj.ConsentName = item.ConsentName;
                consentDocumentObj.ConsentPurpose = item.ConsentPurpose;
                consentDocumentObj.DocumentDisplayName = item.DocumentDisplayName;
                consentDocumentObj.TemplateId = template.TenantId;
                consentDocumentObj.ConsentId = GenerateUniqueId();
                consentDocumentObj.Format = item.Format;
                consentDocumentObj.Version = item.Version;
                consentDocumentObj.ExpiresOn = new TimeBucket(item.ExpiresOn);
                consentDocumentObj.ValidFrom = new TimeBucket(item.ValidFrom);
                consentDocumentObj.Tags = item.Tags;
                consents.Add(consentDocumentObj);

            }

            return consents;
        }

        public async Task Update(string productId, IProductRequestModel productModel)
        {
            ProductValidator.ValidateProductOnUpdate(productId, productModel);

            var productDetails = await ProductRepository.GetProductDetails(productId);
            if(productDetails == null)
                throw new NotFoundException($"Product: {productId} not found");

            if (productDetails.Status == ProductStatus.DeActive)
                throw new NotFoundException($"Product: {productId} is deactive");

            productDetails.Name = productModel.Name;
            productDetails.Description = productModel.Description;
            productDetails.FamilyId = productModel.FamilyId;
            if (productDetails.ValidFrom != null)
                productDetails.ValidFrom = new TimeBucket(productModel.ValidFrom);
            if (productDetails.ExpiresOn != null)
                productDetails.ExpiresOn = new TimeBucket(productModel.ExpiresOn);
            productDetails.PortfolioType = productModel.ProductType;
            productDetails.ProductCategory = productModel.ProductCategory;
            productDetails.IsSecured = productModel.IsSecured;

            var returnCount = await ProductRepository.UpdateProduct(productId, productDetails);

            if (returnCount == 0)
                throw new ArgumentException($"Error while updating product: {productId} ");

            await EventHub.Publish(new Events.ProductUpdated()
            {
                ProductId = productId,
                Product = productDetails
            });
        }

        public async Task Activate(string productId)
        {
            ProductValidator.ValidateProductId(productId);

            var returnCount = await ProductRepository.ActivateProduct(productId);

            if (returnCount == 0)
                throw new NotFoundException("Product not found");

            await EventHub.Publish(new Events.ProductActivated()
            {
                ProductId = productId
            });
        }

        public async Task Expire(string productId)
        {
            ProductValidator.ValidateProductId(productId);

            var returnCount = await ProductRepository.DeActivateProduct(productId);

            if (returnCount == 0)
                throw new NotFoundException("Product not found");

            await EventHub.Publish(new Events.ProductExpired()
            {
                ProductId = productId
            });
        }

        public async Task AddPaymentHierarchy(string productId, List<PaymentHierarchy> paymentHierarchy)
        {
            ProductValidator.ValidateProductId(productId);

            if (paymentHierarchy == null)
                throw new ArgumentException("paymentHierarchy is required");
            foreach (var item in paymentHierarchy)
            {
                if (string.IsNullOrEmpty(item.Name))
                    throw new ArgumentException($"paymentHierarchy Name is required");
                
                if (Lookup.GetLookupEntry("payment-purpose", item.Purpose.ToLower()) == null)
                    throw new ArgumentException($"{item.Purpose} is not a valid payment purpose type");
            }
            var product = await ProductRepository.GetProductDetails(productId);

            if (product == null)
                throw new NotFoundException("Product not found");

            product.PaymentHierarchy = paymentHierarchy.ToList<IPaymentHierarchy>();

            ProductRepository.Update(product);
        }

        public async Task<List<IPaymentHierarchy>> GetPaymentHierarchy(string productId)
        {
            ProductValidator.ValidateProductId(productId);

            var product = await ProductRepository.GetProductDetails(productId);

            if (product == null)
                throw new NotFoundException("Product not found");

            return product.PaymentHierarchy;

        }

        public async Task Delete(string productId)
        {
            ProductValidator.ValidateProductId(productId);

            var returnCount = await ProductRepository.RemoveProduct(productId);

            if (returnCount == 0)
                throw new NotFoundException("Product not found");

            await EventHub.Publish(new Events.ProductRemoved()
            {
                ProductId = productId
            });
        }

        #endregion

        #region Add, Update, Delete, Upload, Consent Document

        public async Task<bool> AddConsentDocument(string productId, List<IConsentRequest> consentDocument)
        {
            ProductValidator.ValidateProductId(productId);

            var productDetails = await ProductRepository.GetProductDetails(productId);
            if (productDetails.Status == ProductStatus.DeActive)
                throw new NotFoundException("Product is in Deactive");

            if (productDetails == null)
                throw new NotFoundException("Product is not found");
            ProductValidator.ValidateCollection("Consent Document", consentDocument);

            List<IConsentDocument> consentList = new List<IConsentDocument>();
            foreach (var item in consentDocument)
            {
                ProductValidator.ValidateConsentDocument(item);
                ITemplate templateDetails = null;

                ITemplate template = new Template();
                template.Name = item.ConsentName;
                template.Format = item.Format;
                template.Body = item.Body;
                template.Properties = item.Properties;
                template.Title = item.Title;
                template.Version = item.Version;

                templateDetails = await TemplateManagerService.Add(template);


                IConsentDocument consentDocumentObj = new ConsentDocument();
                consentDocumentObj.ConsentCategory = item.ConsentCategory;
                consentDocumentObj.ConsentName = item.ConsentName;
                consentDocumentObj.ConsentPurpose = item.ConsentPurpose;
                consentDocumentObj.DocumentDisplayName = item.DocumentDisplayName;
                consentDocumentObj.TemplateId = templateDetails.Id;
                consentDocumentObj.ConsentId = GenerateUniqueId();
                consentDocumentObj.Format = item.Format;
                consentDocumentObj.Version = item.Version;
                consentDocumentObj.ExpiresOn = new TimeBucket(item.ExpiresOn);
                consentDocumentObj.ValidFrom = new TimeBucket(item.ValidFrom);
                consentDocumentObj.Tags = item.Tags;
                consentList.Add(consentDocumentObj);

            }

            if (consentList.Count > 0)
                await ProductRepository.AddConsentDocument(productId, consentList);

            return true;
        }

        public async Task UpdateConsentDocument(string productId, string version, Format format, IConsentRequest consentDocument)
        {
            ProductValidator.ValidateProductId(productId);

            if (consentDocument == null)
                throw new ArgumentNullException(nameof(consentDocument));
            var productDetails = await ProductRepository.GetProductDetails(productId);
            if (productDetails.Status == ProductStatus.DeActive)
                throw new NotFoundException("Product is in Deactive");

            if (productDetails == null)
                throw new NotFoundException("Product not found");

            List<IConsentDocument> consentList = new List<IConsentDocument>();
            consentList = productDetails.Documents;

            var consentDetails = consentList.Where(i => i.ConsentName == consentDocument.ConsentName && i.Format == format && i.Version == version).FirstOrDefault();

            if (consentDetails == null)
                throw new InvalidOperationException($"Template is not found for given parameter consentName {consentDocument.ConsentName}, Version {version} , Format {format}");

            ProductValidator.ValidateConsentDocument(consentDocument);

            ITemplate template = new Template();
            template.Name = consentDocument.ConsentName;
            template.Format = consentDocument.Format;
            template.Body = consentDocument.Body;
            template.Properties = consentDocument.Properties;
            template.Title = consentDocument.Title;
            template.Version = consentDocument.Version;

            var templateDetails = await TemplateManagerService.Get(consentDocument.ConsentName, version, format);

            if (templateDetails == null)
                throw new InvalidOperationException($"Template is not found for given parameter consentName {consentDocument.ConsentName}, Version {version} , Format {format}");

            await Task.Run(() => TemplateManagerService.Update(consentDocument.ConsentName, version, format, template));


            consentDetails.ConsentCategory = consentDocument.ConsentCategory;
            consentDetails.ConsentName = consentDocument.ConsentName;
            consentDetails.ConsentPurpose = consentDocument.ConsentPurpose;
            consentDetails.DocumentDisplayName = consentDocument.DocumentDisplayName;
            consentDetails.TemplateId = templateDetails.Id;
            consentDetails.Version = consentDocument.Version;
            consentDetails.Format = consentDocument.Format;
            consentDetails.ExpiresOn = new TimeBucket(consentDocument.ExpiresOn);
            consentDetails.ValidFrom = new TimeBucket(consentDocument.ValidFrom);
            consentDetails.Tags = consentDocument.Tags;
            var modifiedCount = await ProductRepository.UpdateConsentDocument(productId, consentList);

        }

        public async Task DeleteConsentDocument(string productId, string consentName, string version, Format format)
        {
            if (string.IsNullOrEmpty(productId))
                throw new ArgumentNullException(nameof(productId));

            if (string.IsNullOrEmpty(consentName))
                throw new ArgumentNullException(nameof(consentName));

            if (string.IsNullOrEmpty(version))
                throw new ArgumentNullException(nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            var productDetails = await ProductRepository.GetProductDetails(productId);
            if (productDetails == null)
                throw new NotFoundException("Product not found");


            if (productDetails.Documents != null)
            {
                var consentDetails = productDetails.Documents.Where(i => i.ConsentName.ToLower() == consentName.ToLower() && i.Version == version && i.Format == format).FirstOrDefault();

                if (consentDetails == null)
                    throw new NotFoundException($"Consent document is not found for given parameter consentName {consentName}, Version {version} , Format {format}");

                var template = await TemplateManagerService.Get(consentDetails.ConsentName, version, consentDetails.Format);
                if (template == null)
                    throw new InvalidOperationException($"Template is not found for given parameter consentName {consentDetails.ConsentName}, Version {consentDetails.Version} , Format {consentDetails.Format}");

                await TemplateManagerService.Delete(consentDetails.ConsentName, version, consentDetails.Format);

                productDetails.Documents.Remove(consentDetails);
                await ProductRepository.UpdateProduct(productId, productDetails);
            }
        }

        public async Task<ITemplate> GetTemplateByProduct(string productId, string consentName, string version, Format format)
        {
            if (string.IsNullOrEmpty(productId))
                throw new ArgumentNullException(nameof(productId));

            if (string.IsNullOrEmpty(consentName))
                throw new ArgumentNullException(nameof(consentName));

            if (string.IsNullOrEmpty(version))
                throw new ArgumentNullException(nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            var productDetails = await ProductRepository.GetProductDetails(productId);
            if (productDetails == null)
                throw new NotFoundException("Product not found");

            ITemplate template = null;
            if (productDetails.Documents != null)
            {
                var consentDetails = productDetails.Documents.Where(i => i.ConsentName.ToLower() == consentName.ToLower() && i.Version == version && i.Format == format).FirstOrDefault();

                if (consentDetails == null)
                    throw new NotFoundException($"Consent document is not found for given parameter consentName {consentName}, Version {version} , Format {format}");

                template = await TemplateManagerService.Get(consentDetails.ConsentName, version, consentDetails.Format);
                if (template == null)
                    throw new InvalidOperationException($"Template is not found for given parameter consentName {consentDetails.ConsentName}, Version {consentDetails.Version} , Format {consentDetails.Format}");
                else
                    return template;

            }


            return template;

        }

        public async Task<List<IConsentDocument>> GetAllConsentDocument(string productId)
        {
            if (string.IsNullOrEmpty(productId))
                throw new ArgumentNullException(nameof(productId));

            var productDetails = await ProductRepository.GetProductDetails(productId);

            if (productDetails == null)
                throw new NotFoundException("Product is not found");

            if (productDetails.Documents == null && !productDetails.Documents.Any())
                throw new NotFoundException($"consent documents not found for productId {productId}");

            return productDetails.Documents;

        }

        #endregion

        #region ProductGroup

        public async Task<IProductParameterGroup> AddProductGroup(string productId, IProductGroupRequest productGroupRequest)
        {
            if (productGroupRequest == null)
                throw new ArgumentNullException(nameof(productGroupRequest));

            if (string.IsNullOrEmpty(productId))
                throw new ArgumentNullException(nameof(productId));

            if (string.IsNullOrEmpty(productGroupRequest.GroupId))
                throw new ArgumentNullException(nameof(productGroupRequest.GroupId));

            if (string.IsNullOrEmpty(productGroupRequest.Name))
                throw new ArgumentNullException(nameof(productGroupRequest.Name));

            var productDetails = await ProductRepository.GetProductDetails(productId);
            if (productDetails == null)
                throw new NotFoundException("Product not found");

            var productGroupAlreadyExists = await ProductGroupRepository.GetById(productId, productGroupRequest.GroupId);

            if (productGroupAlreadyExists != null)
                throw new InvalidOperationException($"ProductGroup Id is already exists {productGroupRequest.Name}");


            IProductParameterGroup productGroupModel = new ProductParameterGroup();
            productGroupModel.ProductId = productId;
            productGroupModel.GroupId = productGroupRequest.GroupId;
            productGroupModel.Name = productGroupRequest.Name;
            productGroupModel.Version = productGroupRequest.Version;
            productGroupModel.Description = productGroupRequest.Description;
            productGroupModel.ValidFrom = new TimeBucket(productGroupRequest.ExpiresOn);
            productGroupModel.ExpiresOn = new TimeBucket(productGroupRequest.ValidFrom);
            productGroupModel.CreatedOn = new TimeBucket(TenantTime.Now);
            await Task.Run(() => ProductGroupRepository.Add(productGroupModel));

            return productGroupModel;
        }

        public async Task UpdateProductGroup(string productGroupId, IProductGroupRequest productGroupRequest)
        {
            if (productGroupRequest == null)
                throw new ArgumentNullException(nameof(productGroupRequest));

            if (string.IsNullOrEmpty(productGroupId))
                throw new ArgumentNullException(nameof(productGroupId));

            var productGroupDetails = await ProductGroupRepository.Get(productGroupId);

            if (productGroupDetails == null)
                throw new NotFoundException($"Product Group {productGroupId} not found");


            productGroupDetails.Name = productGroupRequest.Name;
            productGroupDetails.Version = productGroupRequest.Version;
            productGroupDetails.Description = productGroupRequest.Description;
            productGroupDetails.ValidFrom = new TimeBucket(productGroupRequest.ExpiresOn);
            productGroupDetails.ExpiresOn = new TimeBucket(productGroupRequest.ValidFrom);
            productGroupDetails.UpdatedOn = new TimeBucket(TenantTime.Now);

            await Task.Run(() => ProductGroupRepository.Update(productGroupDetails));


        }

        public async Task DeleteProductGroup(string productGroupId)
        {
            if (string.IsNullOrEmpty(productGroupId))
                throw new ArgumentNullException(nameof(productGroupId));

            var productGroup = await ProductGroupRepository.Get(productGroupId);

            if (productGroup == null)
                throw new NotFoundException($"ProductGroup {productGroupId} not found");


            await Task.Run(() => ProductGroupRepository.Remove(productGroup));

        }

        public async Task<IProductParameterGroup> GetProductGroup(string productGroupId)
        {
            if (string.IsNullOrEmpty(productGroupId))
                throw new ArgumentNullException(nameof(productGroupId));

            var productGroupDetails = await ProductGroupRepository.GetProductDetails(productGroupId);

            return productGroupDetails;

        }

        public async Task<List<IProductParameters>> GetProductParameterByGroupName(string productId, string productGroupName)
        {
            if (string.IsNullOrEmpty(productGroupName))
                throw new ArgumentNullException(nameof(productGroupName));

            var productGroupDetails = await ProductGroupRepository.GetByName(productId, productGroupName);

            if (productGroupDetails == null)
                throw new NotFoundException($"Product Group not found for ProductId {productId} and productGroupName {productGroupName} ");

            return await ProductParametersRepository.GetProductParameterDetailsByGroupId(productGroupDetails.Id);

        }

        public async Task<List<IProductParameterGroup>> GetAllProductGroup(string productId)
        {
            if (string.IsNullOrEmpty(productId))
                throw new ArgumentNullException(nameof(productId));

            return await ProductGroupRepository.GetAllByProductId(productId);
        }

        #endregion

        #region ProductParameters

        public async Task<IProductParameters> AddProductParameters(string productId, IProductParametersRequest productParametersRequest)
        {
            if (productParametersRequest == null)
                throw new ArgumentNullException(nameof(productParametersRequest));

            if (string.IsNullOrEmpty(productId))
                throw new ArgumentNullException(nameof(productId));

            if (string.IsNullOrEmpty(productParametersRequest.Name))
                throw new ArgumentNullException(nameof(productParametersRequest.Name));

            if (string.IsNullOrEmpty(productParametersRequest.ParameterType))
                throw new ArgumentNullException(nameof(productParametersRequest.ParameterType));

            if (Lookup.GetLookupEntry("parameter-type", productParametersRequest.ParameterType.ToLower()) == null)
                throw new ArgumentException($"{productParametersRequest.ParameterType} is not a valid parameter type");


            var productDetails = await ProductRepository.GetProductDetails(productId);
            if (productDetails == null)
                throw new NotFoundException("Product not found");

            var productParameterAlreadyExists = await ProductParametersRepository.GetByName(productId, productParametersRequest.Name);

            if (productParameterAlreadyExists != null)
                throw new InvalidOperationException($"ProductParameter Name is already exists {productParametersRequest.Name}");


            IProductParameters productParameterModel = new ProductParameters();
            productParameterModel.ProductId = productId;
            productParameterModel.ParameterId = productParametersRequest.ParameterId;
            productParameterModel.ApplicableEvents = productParametersRequest.ApplicableEvents;
            productParameterModel.Name = productParametersRequest.Name;
            productParameterModel.ApplicableRuleId = productParametersRequest.ApplicableRuleId;
            productParameterModel.Description = productParametersRequest.Description;
            productParameterModel.CalculationRuleId = productParametersRequest.CalculationRuleId;
            productParameterModel.Value = productParametersRequest.Value;
            productParameterModel.UnitValue = productParametersRequest.UnitValue;
            productParameterModel.ParameterType = productParametersRequest.ParameterType;
            productParameterModel.Purpose = productParametersRequest.Purpose;
            productParameterModel.GroupId = productParametersRequest.GroupId;
            productParameterModel.LookUpForValue = productParametersRequest.LookUpValue;
            productParameterModel.LookUpForUnit = productParametersRequest.LookUpValueType;
            productParameterModel.IsSystem = productParametersRequest.IsSystem;
            await Task.Run(() => ProductParametersRepository.Add(productParameterModel));

            return productParameterModel;
        }

        public async Task UpdateProductParameters(string productParameterId, IProductParametersRequest productParameterRequest)
        {
            if (productParameterRequest == null)
                throw new ArgumentNullException(nameof(productParameterRequest));

            if (string.IsNullOrEmpty(productParameterId))
                throw new ArgumentNullException(nameof(productParameterId));

            var productParameterDetails = await ProductParametersRepository.Get(productParameterId);

            if (productParameterDetails == null)
                throw new NotFoundException($"Product Parameter {productParameterId} not found");
            if (Lookup.GetLookupEntry("parameter-type", productParameterRequest.ParameterType) == null)
                throw new ArgumentException($"{productParameterRequest.ParameterType} is not a valid parameter type");

            if (!productParameterDetails.IsSystem)
                productParameterDetails.Name = productParameterRequest.Name;
            productParameterDetails.Value = productParameterRequest.Value;
            productParameterDetails.ApplicableEvents = productParameterRequest.ApplicableEvents;
            productParameterDetails.CalculationRuleId = productParameterRequest.CalculationRuleId;
            productParameterDetails.Description = productParameterRequest.Description;
            productParameterDetails.ApplicableRuleId = productParameterRequest.ApplicableRuleId;
            productParameterDetails.GroupId = productParameterRequest.GroupId;
            productParameterDetails.ParameterType = productParameterRequest.ParameterType;
            productParameterDetails.Purpose = productParameterRequest.Purpose;
            productParameterDetails.LookUpForValue = productParameterRequest.LookUpValue;
            productParameterDetails.LookUpForUnit = productParameterRequest.LookUpValueType;
            await Task.Run(() => ProductParametersRepository.Update(productParameterDetails));
        }

        public async Task DeleteProductParameters(string productParameterId)
        {
            if (string.IsNullOrEmpty(productParameterId))
                throw new ArgumentNullException(nameof(productParameterId));

            var productParametersDetails = await ProductParametersRepository.Get(productParameterId);

            if (productParametersDetails == null)
                throw new NotFoundException($"Product Parameter {productParameterId} not found");
            if (productParametersDetails.IsSystem)
                throw new InvalidOperationException($"Can not delete System parameter");

            await Task.Run(() => ProductParametersRepository.Remove(productParametersDetails));

        }

        public async Task<IProductParameters> GetProductParameter(string productParameterId)
        {
            if (string.IsNullOrEmpty(productParameterId))
                throw new ArgumentNullException(nameof(productParameterId));

            return await ProductParametersRepository.Get(productParameterId);

        }

        public async Task<List<IProductParameters>> GetAllProductParameters(string productId)
        {
            if (string.IsNullOrEmpty(productId))
                throw new ArgumentNullException(nameof(productId));

            return await ProductParametersRepository.GetAllByProductId(productId);
        }

        #endregion

        #region ProductFeeParameters

        public async Task<IProductFeeParameters> AddProductFeeParameters(string productId, IProductFeeParametersRequest productFeeParametersRequest)
        {
            if (productFeeParametersRequest == null)
                throw new ArgumentNullException(nameof(productFeeParametersRequest));

            if (string.IsNullOrEmpty(productId))
                throw new ArgumentNullException(nameof(productId));

            if (string.IsNullOrEmpty(productFeeParametersRequest.Name))
                throw new ArgumentNullException(nameof(productFeeParametersRequest.Name));

            if (string.IsNullOrEmpty(productFeeParametersRequest.FeeId))
                throw new ArgumentNullException(nameof(productFeeParametersRequest.FeeId));

            if (Lookup.GetLookupEntry("feetypes", productFeeParametersRequest.FeeType) == null)
                throw new ArgumentException($"{productFeeParametersRequest.FeeType} is not a valid parameter type");

            if (!string.IsNullOrEmpty(productFeeParametersRequest.FrequencyType))
            {
                if (Lookup.GetLookupEntry("frequency-type", productFeeParametersRequest.FrequencyType) == null)
                    throw new ArgumentException($"{productFeeParametersRequest.FrequencyType} is not a valid parameter type");
            }

            if (!Enum.IsDefined(typeof(AppliedOnType), productFeeParametersRequest.AppliedOnType))
                throw new ArgumentOutOfRangeException(nameof(productFeeParametersRequest.AppliedOnType));

            var productDetails = await ProductRepository.GetProductDetails(productId);

            if (productDetails == null)
                throw new NotFoundException("Product not found");

            var productFeeParameterAlreadyExists = await ProductFeeParametersRepository.GetByFeeId(productId, productFeeParametersRequest.FeeId);

            if (productFeeParameterAlreadyExists != null)
                throw new InvalidOperationException($"ProductFeeParameter Id is already exists {productFeeParametersRequest.Name}");


            IProductFeeParameters productFeeParameterModel = new ProductFeeParameters();
            productFeeParameterModel.ProductId = productId;
            productFeeParameterModel.Name = productFeeParametersRequest.Name;
            productFeeParameterModel.FeeId = productFeeParametersRequest.FeeId;
            productFeeParameterModel.ApplicableEvents = productFeeParametersRequest.Events;
            productFeeParameterModel.AutoPay = productFeeParametersRequest.AutoPay;
            productFeeParameterModel.ApplicableRule = productFeeParametersRequest.ApplicableRule;
            productFeeParameterModel.Description = productFeeParametersRequest.Description;
            productFeeParameterModel.GroupId = productFeeParametersRequest.GroupId;
            productFeeParameterModel.CalculationRule = productFeeParametersRequest.CalculationRule;
            productFeeParameterModel.FeeAmount = productFeeParametersRequest.FeeAmount;
            productFeeParameterModel.AppliedOnType = productFeeParametersRequest.AppliedOnType;
            productFeeParameterModel.FeeType = productFeeParametersRequest.FeeType;
            productFeeParameterModel.FeePaymentFrequency = productFeeParametersRequest.FrequencyType;
            productFeeParameterModel.IsAccrue = productFeeParametersRequest.IsAccrue;
            productFeeParameterModel.IsSystem = productFeeParametersRequest.IsSystem;
            await Task.Run(() => ProductFeeParametersRepository.AddOrUpdate(productFeeParameterModel));

            return productFeeParameterModel;
        }

        public async Task UpdateProductFeeParameters(string productFeeParameterId, IProductFeeParametersRequest productFeeParametersRequest)
        {
            if (productFeeParametersRequest == null)
                throw new ArgumentNullException(nameof(productFeeParametersRequest));

            if (string.IsNullOrEmpty(productFeeParameterId))
                throw new ArgumentNullException(nameof(productFeeParameterId));

            var productFeeParameterDetails = await ProductFeeParametersRepository.Get(productFeeParameterId);

            if (productFeeParameterDetails == null)
                throw new NotFoundException($"Product Fee Parameter {productFeeParameterId} not found");
            if (Lookup.GetLookupEntry("feetypes", productFeeParametersRequest.FeeType) == null)
                throw new ArgumentException($"{productFeeParametersRequest.FeeType} is not a valid parameter type");

            if (Lookup.GetLookupEntry("frequency-type", productFeeParametersRequest.FeeType) == null)
                throw new ArgumentException($"{productFeeParametersRequest.FrequencyType} is not a valid parameter type");



            productFeeParameterDetails.Name = productFeeParametersRequest.Name;
            productFeeParameterDetails.ApplicableRule = productFeeParametersRequest.ApplicableRule;
            productFeeParameterDetails.Description = productFeeParametersRequest.Description;
            productFeeParameterDetails.CalculationRule = productFeeParametersRequest.CalculationRule;
            productFeeParameterDetails.ApplicableEvents = productFeeParametersRequest.Events;
            productFeeParameterDetails.GroupId = productFeeParametersRequest.GroupId;
            productFeeParameterDetails.AutoPay = productFeeParametersRequest.AutoPay;
            productFeeParameterDetails.FeeAmount = productFeeParametersRequest.FeeAmount;
            productFeeParameterDetails.AppliedOnType = productFeeParametersRequest.AppliedOnType;
            productFeeParameterDetails.FeeType = productFeeParametersRequest.FeeType;
            productFeeParameterDetails.FeePaymentFrequency = productFeeParametersRequest.FrequencyType;
            productFeeParameterDetails.IsAccrue = productFeeParametersRequest.IsAccrue;
            productFeeParameterDetails.IsSystem = productFeeParametersRequest.IsSystem;
            await Task.Run(() => ProductFeeParametersRepository.AddOrUpdate(productFeeParameterDetails));
        }

        public async Task DeleteProductFeeParameters(string productFeeParameterId)
        {
            if (string.IsNullOrEmpty(productFeeParameterId))
                throw new ArgumentNullException(nameof(productFeeParameterId));

            var productFeeParametersDetails = await ProductFeeParametersRepository.Get(productFeeParameterId);

            if (productFeeParametersDetails == null)
                throw new NotFoundException($"Product Parameter {productFeeParameterId} not found");


            await Task.Run(() => ProductFeeParametersRepository.Remove(productFeeParametersDetails));

        }

        public async Task<IProductFeeParameters> GetProductFeeParameter(string productFeeParameterId)
        {
            if (string.IsNullOrEmpty(productFeeParameterId))
                throw new ArgumentNullException(nameof(productFeeParameterId));

            return await ProductFeeParametersRepository.Get(productFeeParameterId);

        }

        public async Task<List<IProductFeeParameters>> GetAllProductFeeParameters(string productId)
        {
            if (string.IsNullOrEmpty(productId))
                throw new ArgumentNullException(nameof(productId));

            return await ProductFeeParametersRepository.GetAllByProductId(productId);
        }

        public async Task<List<IProductFeeParameters>> GetProductFeeParameterByGroupName(string productId, string groupName)
        {
            if (string.IsNullOrEmpty(groupName))
                throw new ArgumentNullException(nameof(groupName));

            var productGroupDetails = await ProductGroupRepository.GetByName(productId, groupName);

            if (productGroupDetails == null)
                throw new NotFoundException($"Product Group not found for ProductId {productId} and productGroupName {groupName} ");

            return await ProductFeeParametersRepository.GetProductParameterDetailsByGroupId(productGroupDetails.GroupId);

        }

        #endregion

        #region StatusWorkFlow - Add update delete get

        public async Task<IProduct> AddStatusWorkFlow(string productId, IWorkFlowRequest workFlowRequest)
        {
            ProductValidator.ValidateProductId(productId);

            if (workFlowRequest == null)
                throw new ArgumentNullException(nameof(workFlowRequest));

            ProductValidator.ValidateWorkFlow(workFlowRequest);

            var productDetails = await ProductRepository.GetProductDetails(productId);

            if (productDetails.Status == ProductStatus.DeActive)
                throw new NotFoundException("Product is in Deactive");
            if (productDetails == null)
                throw new NotFoundException("Product is not found");

            var lastWorkFlow = productDetails.WorkFlow ?? new List<IWorkFlow>();

            if (lastWorkFlow.Any(x => x.WorkFlowName == workFlowRequest.WorkFlowName && x.EntityType == workFlowRequest.EntityType))
                throw new InvalidOperationException($"Workflow with name {workFlowRequest.WorkFlowName} is already exists for entityType {workFlowRequest.EntityType}");

            var workflowName = await EntityStatusService.GetWorkFlowNames(workFlowRequest.EntityType);

            if (workflowName != null && !workflowName.Any(x => x == workFlowRequest.WorkFlowName))
                throw new NotFoundException($"workflow Name {workFlowRequest.WorkFlowName} is not found in status-management");

            var templateDetail = ProductTemplateRepository.GetProductTemplate(productDetails.PortfolioType).Result;
            if (templateDetail == null)
                throw new InvalidOperationException($"Product Template details does not found for portfolioType {productDetails.PortfolioType.ToString()}");

            JObject objContent = (JObject)JsonConvert.DeserializeObject(templateDetail.Template.ToString());
            ValidateWorkFlowType(objContent, workFlowRequest.WorkFlowType.ToString(), productDetails.PortfolioType.ToString());
            ValidateEntityType(objContent, workFlowRequest.EntityType.ToString(), productDetails.PortfolioType.ToString());

            var workFlow = new WorkFlow();
            workFlow.Id = GenerateUniqueId();
            workFlow.EntityType = workFlowRequest.EntityType;
            workFlow.Description = workFlowRequest.Description;
            workFlow.ValidFrom = new TimeBucket(workFlowRequest.ValidFrom);
            workFlow.ExpiresOn = new TimeBucket(workFlowRequest.ExpiresOn);
            workFlow.WorkFlowName = workFlowRequest.WorkFlowName;
            workFlow.WorkFlowType = workFlowRequest.WorkFlowType;
            lastWorkFlow.Add(workFlow);

            productDetails.WorkFlow = lastWorkFlow;
            await ProductRepository.UpdateProduct(productId, productDetails);
            return productDetails;
        }

        public async Task<IProduct> UpdateStatusWorkFlow(string productId, string workFlowId, IWorkFlowRequest workFlowRequest)
        {
            ProductValidator.ValidateProductId(productId);

            if (workFlowRequest == null)
                throw new ArgumentNullException(nameof(workFlowRequest));

            ProductValidator.ValidateWorkFlow(workFlowRequest);

            var productDetails = await ProductRepository.GetProductDetails(productId);
            if (productDetails.Status == ProductStatus.DeActive)
                throw new NotFoundException("Product is in Deactive");
            if (productDetails == null)
                throw new NotFoundException("Product is not found");
            var lastWorkFlow = productDetails.WorkFlow ?? new List<IWorkFlow>();

            var oldWorkFlow = lastWorkFlow.FirstOrDefault(a => a.Id == workFlowId);
            if (oldWorkFlow == null)
                throw new NotFoundException($"Status WorkFlow with id {workFlowId} for Product {productId} not found");

            var templateDetail = ProductTemplateRepository.GetProductTemplate(productDetails.PortfolioType).Result;
            if (templateDetail == null)
                throw new InvalidOperationException($"Product Template details does not found for portfolioType {productDetails.PortfolioType.ToString()}");

            JObject objContent = (JObject)JsonConvert.DeserializeObject(templateDetail.Template.ToString());
            ValidateWorkFlowType(objContent, workFlowRequest.WorkFlowType.ToString(), productDetails.PortfolioType.ToString());
            ValidateEntityType(objContent, workFlowRequest.EntityType.ToString(), productDetails.PortfolioType.ToString());

            oldWorkFlow.WorkFlowType = workFlowRequest.WorkFlowType;
            oldWorkFlow.Description = workFlowRequest.Description;
            oldWorkFlow.ExpiresOn = new TimeBucket(workFlowRequest.ExpiresOn);
            oldWorkFlow.ValidFrom = new TimeBucket(workFlowRequest.ValidFrom);

            productDetails.WorkFlow = lastWorkFlow;
            await ProductRepository.UpdateProduct(productId, productDetails);

            return productDetails;
        }

        public async Task DeleteStatusWorkFlow(string productId, string workFlowId)
        {
            ProductValidator.ValidateProductId(productId);

            if (string.IsNullOrEmpty(workFlowId))
                throw new ArgumentNullException(nameof(workFlowId));

            var productDetails = await ProductRepository.GetProductDetails(productId);
            if (productDetails == null)
                throw new NotFoundException("Product is not found");

            var lastWorkFlow = productDetails.WorkFlow ?? new List<IWorkFlow>();

            var oldWorkFlow = lastWorkFlow.FirstOrDefault(a => a.Id == workFlowId);
            if (oldWorkFlow == null)
                throw new NotFoundException($"Status WorkFlow with id {workFlowId} for Product {productId} not found");
            productDetails.WorkFlow.Remove(oldWorkFlow);

            await ProductRepository.UpdateProduct(productId, productDetails);

        }

        public async Task<List<IWorkFlow>> GetAllStatusWorkFlow(string productId)
        {
            ProductValidator.ValidateProductId(productId);

            var productDetails = await ProductRepository.GetProductDetails(productId);
            if (productDetails == null)
                throw new NotFoundException("Product is not found");

            if (productDetails.WorkFlow == null && !productDetails.WorkFlow.Any())
                throw new NotFoundException($"Workflow not found for productId {productId}");

            return productDetails.WorkFlow;

        }

        public async Task<IWorkFlow> GetWorkFlowByName(string productId, string workFlowName)
        {
            if (string.IsNullOrEmpty(productId))
                throw new ArgumentNullException(nameof(productId));

            if (string.IsNullOrEmpty(workFlowName))
                throw new ArgumentNullException(nameof(workFlowName));

            var productDetails = await ProductRepository.GetProductDetails(productId);

            if (productDetails == null)
                throw new NotFoundException("Product is not found");
            var consentDetail = productDetails.WorkFlow.FirstOrDefault(x => x.WorkFlowName == workFlowName);
            if (consentDetail == null)
                throw new NotFoundException($"WorkFlow with name {workFlowName} not found under product {productId}");

            return consentDetail;

        }
        
        #endregion

        #region Product-Template

        public async Task<IProductTemplate> GetProductTemplate(PortfolioType portfolioType)
        {
            return await ProductTemplateRepository.GetProductTemplate(portfolioType);
        }
        public async Task<IProductTemplate> AddTemplateDetail(IProductTemplateRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (request.Template == null)
                throw new ArgumentNullException(nameof(request.Template));

            var templateData = new ProductTemplate(request);
            await Task.Run(() => ProductTemplateRepository.AddOrUpdate(templateData));
            return templateData;
        }

        #endregion

        private static string GenerateUniqueId()
        {
            return Guid.NewGuid().ToString("N");
        }


    }
}
