﻿using LendFoundry.Foundation.Date;
using System;

namespace LendFoundry.ProductConfiguration
{
    public static class ProductExtension
    {
        public static IProduct ToProduct(this IProductRequestModel productRequest)
        {
            var product = new Product();

            product.ProductId = productRequest.ProductId.Replace(" ", String.Empty).ToLower(); 
            product.Name = productRequest.Name;
            product.Description = productRequest.Description;
            product.FamilyId = productRequest.FamilyId;
            product.Status =ProductStatus.Active;
            product.ValidFrom =new TimeBucket(productRequest.ValidFrom);
            product.ExpiresOn = new TimeBucket(productRequest.ExpiresOn);
            product.PortfolioType = productRequest.ProductType;
            product.ProductCategory = productRequest.ProductCategory;
            product.IsSecured = productRequest.IsSecured;
            return product;
        }
    }
}
