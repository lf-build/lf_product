﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public static class ProductValidator
    {
        public static void ValidateProductId(string productId)
        {
            if (String.IsNullOrEmpty(productId))
                throw new ArgumentException("Product Id should not empty.");
        }

        public static void ValidateCollection<t>(string name, List<t> collection)
        {
            if (collection == null || !collection.Any())
                throw new ArgumentNullException(name + " should not empty.");
        }

        public static void ValidateProductOnAdd(IProductRequestModel productModel)
        {
            ValidateProductElement(productModel);
           
        }

        public static void ValidateProductOnUpdate(string productId, IProductRequestModel productModel)
        {
            ValidateProductElement(productModel);

            if (String.IsNullOrEmpty(productId))
                throw new ArgumentNullException("Product Id cannot be empty.");
          
        }

        public static void ValidateProductElement(IProductRequestModel productModel)
        {
            if (productModel == null)
                throw new ArgumentNullException(nameof(productModel));

            if (String.IsNullOrEmpty(productModel.Name))
                throw new ArgumentNullException("Product name cannot be empty.");

            if (String.IsNullOrEmpty(productModel.Description))
                throw new ArgumentNullException("Product description cannot be empty.");

            if (String.IsNullOrEmpty(productModel.FamilyId))
                throw new ArgumentNullException("Family Id cannot be empty.");
        }

       
        public static void ValidateInterest(IInterest model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            //if (String.IsNullOrEmpty(model.InterestType))
            //    throw new ArgumentException("Please provide Interest Type");

            //if (model.CompoundingPeriod == 0)
            //    throw new ArgumentException("Please provide Compouding Period");

            //if (String.IsNullOrEmpty(model.TermDuration))
            //    throw new ArgumentException("Please provide Term Duration");

            if (model.SupportedLoanTerms == null || !model.SupportedLoanTerms.Any())
                throw new ArgumentException("Please provide Supported Loan Terms");

            //if (String.IsNullOrEmpty(model.PaymentFrequency))
            //    throw new ArgumentException("Please provide Payment Frequency");

            //if (String.IsNullOrEmpty(model.PaymentType))
            //    throw new ArgumentException("Please provide Payment Type");

            //if (String.IsNullOrEmpty(model.PaymentTime))
            //    throw new ArgumentException("Please provide Payment Time");

            //if (String.IsNullOrEmpty(model.AmortizationCalendar))
            //    throw new ArgumentException("Please provide Amortization Calendar");
        }

        public static void ValidateCharter(ICharter model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (Convert.ToString(model.BankId) == "" || model.BankId == 0)
                throw new ArgumentException("Please provide Bank Id");

            ValidateGeographiesElement(model.SupportedGeographies);
        }

        public static void ValidateFeesCharge(IFeesCharge model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (string.IsNullOrEmpty(model.FeeDescription))
                throw new ArgumentException("Please provide Fee Description");

            if (model.StartDate == null)
                throw new ArgumentException("Please provide StartDate");

            if (model.ExpiresOn == null)
                throw new ArgumentException("Please provide ExpiresOn");

            ValidateRuleElement(model.FeeAmount);

            ValidateRuleElement(model.Applicability);

           
        }

        public static void ValidateUsuryRules(IUsuryRules model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (string.IsNullOrEmpty(model.RulePurpose))
                throw new ArgumentException("Please provide Usury Rules Purpose");

            if (string.IsNullOrEmpty(model.Rule))
                throw new ArgumentException("Please provide Usury Rules Purpose");

            if (model.StartDate == null)
                throw new ArgumentException("Please provide StartDate");

            if (model.ExpiresOn == null)
                throw new ArgumentException("Please provide ExpiresOn");

            ValidateGeographiesElement(model.ApplicableGeography);
        }

        public static void ValidateUploadDocument(IDocument model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (string.IsNullOrEmpty(model.Name))
                throw new ArgumentException("File name should not be empty");

            if (model.Stream == null)
                throw new ArgumentException("File not available to upload");
        }

        public static void ValidateWorkFlow(IWorkFlowRequest model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (string.IsNullOrEmpty(model.WorkFlowName))
                throw new ArgumentException("Please provide ConsentName");

            if (!Enum.IsDefined(typeof(WorkFlowType), model.WorkFlowType))
                throw new ArgumentNullException($" {nameof(model.WorkFlowType)} is mandatory");


            if (string.IsNullOrWhiteSpace(model.EntityType))
                throw new ArgumentException("Template body is required", nameof(model.EntityType));

           if (model.ValidFrom == null)
                throw new ArgumentException("Please provide StartDate");

            if (model.ExpiresOn == null)
                throw new ArgumentException("Please provide ExpiresOn");
        }
        public static void ValidateConsentDocument(IConsentRequest model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (string.IsNullOrEmpty(model.ConsentName))
                throw new ArgumentException("Please provide ConsentName");

            if (string.IsNullOrEmpty(model.ConsentCategory))
                throw new ArgumentException("Please provide ConsentCategory");
         

            if (string.IsNullOrWhiteSpace(model.Body))
                throw new ArgumentException("Template body is required", nameof(model.Body));

            if (string.IsNullOrWhiteSpace(model.Version))
                throw new ArgumentException("Argument is null or whitespace", nameof(model.Version));

            //if (!Enum.IsDefined(typeof(Format), model.Format))
            //    throw new ArgumentOutOfRangeException(nameof(model.Format));

            if (model.ValidFrom == null)
                throw new ArgumentException("Please provide StartDate");

            if (model.ExpiresOn == null)
                throw new ArgumentException("Please provide ExpiresOn");
        }
       

        public static void ValidateFundingSource(IFundingSource model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (string.IsNullOrEmpty(model.Name))
                throw new ArgumentException("Please provide Funding Source");
        }
        public static void ValidateRuleElement(IRule model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (string.IsNullOrEmpty(model.Name))
                throw new ArgumentException("Please provide Rule Element Name");

            if (string.IsNullOrEmpty(model.Description))
                throw new ArgumentException("Please provide Rule Element Description");
        }

        public static void ValidateGeographiesElement(IGeographies model)
        {
            if (model != null)
            {
                if (model.Country != null)
                {
                    if (string.IsNullOrEmpty(model.Country.Name))
                        throw new ArgumentException("Please provide County Name in Country under Geographies");

                    if (string.IsNullOrEmpty(model.Country.Code))
                        throw new ArgumentException("Please provide County Code in Country under Geographies");
                }

                if (model.IncludedStates != null && model.IncludedStates.Any())
                {
                    model.IncludedStates.ForEach(state =>
                    {
                        if (string.IsNullOrEmpty(state.CountryCode))
                            throw new ArgumentException("Please provide State County Code in Included States under Geographies");

                        if (string.IsNullOrEmpty(state.Code))
                            throw new ArgumentException("Please provide State Code in Included States under Geographies");

                        if (string.IsNullOrEmpty(state.Name))
                            throw new ArgumentException("Please provide State Name in Included States under Geographies");
                    });
                }

                if (model.ExcludedStates != null && model.ExcludedStates.Any())
                {
                    model.ExcludedStates.ForEach(state =>
                    {
                        if (string.IsNullOrEmpty(state.CountryCode))
                            throw new ArgumentException("Please provide State County Code in Excluded States under Geographies");

                        if (string.IsNullOrEmpty(state.Code))
                            throw new ArgumentException("Please provide State Code in Excluded States under Geographies");

                        if (string.IsNullOrEmpty(state.Name))
                            throw new ArgumentException("Please provide State Name in Excluded States under Geographies");
                    });
                }
            }
        }
    }
}
