﻿using System;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson;
using LendFoundry.Foundation.Services;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration.Persistence
{
    public class ProductFeeParametersRepository : MongoRepository<IProductFeeParameters, ProductFeeParameters>, IProductFeeParametersRepository
    {

        static ProductFeeParametersRepository()
        {
            BsonClassMap.RegisterClassMap<ProductFeeParameters>(map =>
            {
                map.AutoMap();

                map.MapMember(p => p.AppliedOnType).SetSerializer(new EnumSerializer<AppliedOnType>(BsonType.String));

                var type = typeof(ProductFeeParameters);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }
        
        private new ITenantService TenantService { get; }
        private ITenantTime TenantTime { get; }

        public ProductFeeParametersRepository(ITenantService tenantService, IMongoConfiguration configuration, ITenantTime tenantTime) : base(tenantService, configuration, "fees")
        {
            TenantService = tenantService;
            TenantTime = tenantTime;
        }

        public async Task<IProductFeeParameters> GetFeeDetailsById(string feeId)
        {
            var productFee = await Query.FirstOrDefaultAsync(row => row.FeeId == feeId);
            if (productFee == null)
                throw new NotFoundException("Product Fee not found");
            else
                return productFee;
        }

        public async Task<IProductFeeParameters> GetByName(string productId, string feeName)
        {
            return await Task.Run(() =>
            {
                return Query.Where(productfee => productfee.ProductId == productId && productfee.Name.ToLower() == feeName.ToLower()).FirstOrDefault();
            });
        }

        public async Task<IProductFeeParameters> GetByFeeId(string productId, string feeId)
        {
            return await Task.Run(() =>
            {
                return Query.Where(productfee => productfee.ProductId == productId && productfee.FeeId == feeId).FirstOrDefault();
            });
        }

        public async Task<List<IProductFeeParameters>> GetAllByProductId(string productId)
        {
            return await Task.Run(() =>
            {
                return Query.Where(productfee => productfee.ProductId == productId).ToListAsync();
            });
        }

        public async Task<List<IProductFeeParameters>> GetProductParameterDetailsByGroupId(string productGroupId)
        {
            var productFeeParameterDetails = await Query.Where(row => row.GroupId == productGroupId).ToListAsync();
            if (productFeeParameterDetails == null)
                throw new NotFoundException("Product Parameters not found");
            else
                return productFeeParameterDetails;
        }

        public void AddOrUpdate(IProductFeeParameters productFeeParameters)
        {
            productFeeParameters.TenantId = TenantService.Current.Id;

            Collection.UpdateOne(s =>
                    s.FeeId == productFeeParameters.FeeId &&
                     s.ProductId == productFeeParameters.ProductId &&
                    s.TenantId == TenantService.Current.Id,
                new UpdateDefinitionBuilder<IProductFeeParameters>()
                 .Set(a => a.ApplicableEvents, productFeeParameters.ApplicableEvents)
                .Set(a => a.ApplicableRule, productFeeParameters.ApplicableRule)
                      .Set(a => a.AppliedOnType, productFeeParameters.AppliedOnType)
                .Set(a => a.AutoPay, productFeeParameters.AutoPay)
                .Set(a => a.CalculationRule, productFeeParameters.CalculationRule)
                .Set(a => a.Description, productFeeParameters.Description)
                .Set(a => a.FeeAmount, productFeeParameters.FeeAmount)
                    .Set(a => a.FeePaymentFrequency, productFeeParameters.FeePaymentFrequency)
                       .Set(a => a.FeeType, productFeeParameters.FeeType)
                       .Set(a => a.GroupId, productFeeParameters.GroupId)
             .Set(a => a.IsAccrue, productFeeParameters.IsAccrue)
              .Set(a => a.Name, productFeeParameters.Name)
               .Set(a => a.IsSystem, productFeeParameters.IsSystem)
                , new UpdateOptions() { IsUpsert = true });



        }

    }
}
