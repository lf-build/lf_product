﻿using System;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson;
using LendFoundry.Foundation.Services;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration.Persistence
{
    public class ProductTemplateRepository : MongoRepository<IProductTemplate, ProductTemplate>, IProductTemplateRepository
    {
        static ProductTemplateRepository()
        {
            BsonClassMap.RegisterClassMap<ProductTemplate>(map =>
            {
                map.AutoMap();                
                map.MapProperty(p => p.PortfolioType).SetSerializer(new EnumSerializer<PortfolioType>(BsonType.String));
                var type = typeof(ProductTemplate);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.MapProperty(s => s.Template).SetSerializer(new BsonJsonSerializer<object>());
                map.SetIsRootClass(true);
            });          
        }
        public ProductTemplateRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "product-template")
        {
            CreateIndexIfNotExists("template_id", Builders<IProductTemplate>.IndexKeys.Ascending(i => i.PortfolioType));
        }


        public async Task<IProductTemplate> GetProductTemplate(PortfolioType portfolioType)
        {
            var productTemplate = await Query.FirstOrDefaultAsync<IProductTemplate>(row => row.PortfolioType == portfolioType);
            return productTemplate;
        }

        public void AddOrUpdate(IProductTemplate productTemplate)
        {
            productTemplate.TenantId = TenantService.Current.Id;

            Collection.UpdateOne(s =>
                     s.PortfolioType == productTemplate.PortfolioType &&
                    s.TenantId == TenantService.Current.Id,
                new UpdateDefinitionBuilder<IProductTemplate>()
                 .Set(a => a.Template, productTemplate.Template)               
                , new UpdateOptions() { IsUpsert = true });
        }


    }
}
