﻿using System;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson;
using LendFoundry.Foundation.Services;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration.Persistence
{
    public class ProductParametersRepository : MongoRepository<IProductParameters, ProductParameters>, IProductParametersRepository
    {  
        private new ITenantService TenantService { get; }
        
        private ITenantTime TenantTime { get; }

        public ProductParametersRepository(ITenantService tenantService, IMongoConfiguration configuration, ITenantTime tenantTime) : base(tenantService, configuration, "parameters")
        {
            TenantService = tenantService;
            TenantTime = tenantTime;
        }

        public async Task<List<IProductParameters>> GetProductParameterDetailsByGroupId(string productGroupId)
        {
            var productParameterDetails =await Query.Where(row => row.GroupId == productGroupId).ToListAsync();
            if (productParameterDetails == null)
                throw new NotFoundException("Product Parameters not found");
            else
                return  productParameterDetails;
        }

        public async Task<IProductParameters> GetByName(string productId, string Name)
        {
            return await Task.Run(() =>
            {
                return Query.Where(productgroup => productgroup.ProductId == productId && productgroup.Name.ToLower() == Name.ToLower()).FirstOrDefault();
            });
        }

        public async Task<IProductParameters> GetByParameterId(string productId, string parameterId)
        {
            return await Task.Run(() =>
            {
                return Query.Where(param => param.ProductId == productId && param.ParameterId == parameterId).FirstOrDefault();
            });
        }

        public async Task<List<IProductParameters>> GetAllByProductId(string productId)
        {
            return await Task.Run(() =>
            {
                return Query.Where(productparameters => productparameters.ProductId == productId).ToListAsync();
            });
        }

    }
}
