﻿using System;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson;
using LendFoundry.Foundation.Services;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration.Persistence
{
    public class ProductGroupRepository : MongoRepository<IProductParameterGroup, ProductParameterGroup>, IProductGroupRepository
    {
        private new ITenantService TenantService { get; }
        private ITenantTime TenantTime { get; }

        public ProductGroupRepository(ITenantService tenantService, IMongoConfiguration configuration, ITenantTime tenantTime) : base(tenantService, configuration, "group")
        {
            TenantService = tenantService;
            TenantTime = tenantTime;
        }

        public async Task<IProductParameterGroup> GetProductDetails(string productGroupId)
        {
            var productGroup =await Query.FirstOrDefaultAsync<IProductParameterGroup>(row => row.GroupId == productGroupId);
            if (productGroup == null)
                throw new NotFoundException("Product Group not found");
            else
                return  productGroup;
        }

        public async Task<IProductParameterGroup> GetByName(string productId, string Name)
        {
            return await Task.Run(() =>
            {
                return Query.Where(productgroup => productgroup.ProductId == productId  && productgroup.Name.ToLower() == Name.ToLower()).FirstOrDefault();
            });
        }

        public async Task<IProductParameterGroup> GetById(string productId, string groupId)
        {
            return await Task.Run(() =>
            {
                return Query.Where(productgroup => productgroup.ProductId == productId && productgroup.GroupId == groupId).FirstOrDefault();
            });
        }

        public async Task<List<IProductParameterGroup>> GetAllByProductId(string productId)
        {
            return await Task.Run(() =>
            {
                return Query.Where(productgroup => productgroup.ProductId == productId).ToListAsync();
            });
        }

    }
}
