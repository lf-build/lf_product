﻿using System;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson;
using LendFoundry.Foundation.Services;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration.Persistence
{
    public class ProductRepository : MongoRepository<IProduct, Product>, IProductRepository
    {
        static ProductRepository()
        {
            BsonClassMap.RegisterClassMap<Product>(map =>
            {
                map.AutoMap();

                map.MapMember(p => p.Status).SetSerializer(new EnumSerializer<ProductStatus>(BsonType.String));
                map.MapProperty(p => p.ProductCategory).SetSerializer(new EnumSerializer<ProductCategory>(BsonType.String));
                map.MapProperty(p => p.PortfolioType).SetSerializer(new EnumSerializer<PortfolioType>(BsonType.String));
                var type = typeof(Product);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<ConsentDocument>(map =>
            {
                map.AutoMap();              
                var type = typeof(ConsentDocument);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<PaymentHierarchy>(map =>
            {
                map.AutoMap();
                var type = typeof(PaymentHierarchy);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<PaymentElement>(map =>
            {
                map.AutoMap();
                var type = typeof(PaymentElement);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<WorkFlow>(map =>
            {
                map.AutoMap();
                var type = typeof(WorkFlow);
                map.MapProperty(p => p.WorkFlowType).SetSerializer(new EnumSerializer<WorkFlowType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            
            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });
        }

        private new ITenantService TenantService { get; }
        private ITenantTime TenantTime { get; }

        public ProductRepository(ITenantService tenantService, IMongoConfiguration configuration, ITenantTime tenantTime) : base(tenantService, configuration, "product")
        {
            TenantService = tenantService;
            TenantTime = tenantTime;
            CreateIndexIfNotExists("productId", Builders<IProduct>.IndexKeys.Ascending(i => i.ProductId));


        }

        public async Task<List<IProduct>> GetAll()
        {
            return await Query.ToListAsync();
        }

        public async Task<IProduct> GetProductDetails(string productId)
        {
            var product =await Query.FirstOrDefaultAsync<IProduct>(row => row.ProductId == productId);           
            return  product;
        }

        #region Add Update Product
        public string AddProduct(IProduct product)
        {
            Add(product);

            return product.Id;
        }

        public async Task<long> UpdateProduct(string productId, IProduct product)
        {
            //TODO: Can we have method in base to return the updateresult?
            var updateResult =await Collection.ReplaceOneAsync<IProduct>((p => p.ProductId.Equals(productId)), product);
            return updateResult.ModifiedCount;
        }

        public async Task<long> ActivateProduct(string productId)
        {
            var filter = Builders<IProduct>.Filter.Eq(row => row.ProductId, productId);

            var updateStatement = new UpdateDefinitionBuilder<IProduct>().
                  Set(row => row.Status, ProductStatus.Active).
                   Set(row => row.ValidFrom,new TimeBucket(TenantTime.Now));

            var updateResult = await Collection.UpdateOneAsync(filter, updateStatement);

            //TODO: Need to look into IsAcknowledged property
            return updateResult.ModifiedCount;
        }

        public async Task<long> DeActivateProduct(string productId)
        {
            var filter = Builders<IProduct>.Filter.Eq(row => row.ProductId, productId);

            var updateStatement = new UpdateDefinitionBuilder<IProduct>().
                    Set(row => row.Status, ProductStatus.DeActive).
                   Set(row => row.ExpiresOn, new TimeBucket(TenantTime.Now));

            var updateResult =await Collection.UpdateOneAsync(filter, updateStatement);

            return updateResult.ModifiedCount;
        }

        public async Task<long> RemoveProduct(string productId)
        {
            var filter = Builders<IProduct>.Filter.Eq(row => row.ProductId, productId);

            var updateStatement = new UpdateDefinitionBuilder<IProduct>().
                   Set(row => row.Status, ProductStatus.DeActive);

            var updateResult =await Collection.UpdateOneAsync(filter, updateStatement);

            return updateResult.ModifiedCount;

        }

        #endregion
        

        #region Add Update Consent Document

        public async Task<long> AddConsentDocument(string productId, List<IConsentDocument> consentDocument)
        {
            var filter = Builders<IProduct>.Filter.Eq(row => row.ProductId, productId);

            var updateStatement = new UpdateDefinitionBuilder<IProduct>().AddToSetEach("Documents", consentDocument);

            var updateResult =await Collection.UpdateOneAsync(filter, updateStatement);

            return updateResult.ModifiedCount;
        }

        public async Task<long> UpdateConsentDocument(string productId, List<IConsentDocument> consentDocument)
        {
            var filter = Builders<IProduct>.Filter.Eq(row => row.ProductId, productId);

            var updateStatement = new UpdateDefinitionBuilder<IProduct>().Set("Documents", consentDocument);

            var updateResult =await Collection.UpdateOneAsync(filter, updateStatement);

            return updateResult.ModifiedCount;
        }
        
        #endregion


    }
}
