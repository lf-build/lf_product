﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
using System.Threading.Tasks;
using RestSharp;
using LendFoundry.TemplateManager;
using System;

namespace LendFoundry.ProductConfiguration.Client
{
    public class ProdutService : IProductService
    {
        public ProdutService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        #region Product

        public async Task<IProduct> Get(string id)
        {
            var request = new RestRequest("product/{id}", Method.GET);
            request.AddUrlSegment("id", id);
            return await Client.ExecuteAsync<Product>(request);

        }

        public async Task<string> Add(IProductRequestModel productModel)
        {
            var request = new RestRequest("/product", Method.PUT);
            request.AddJsonBody(productModel);

            var result = await Client.ExecuteAsync<string>(request);

            return result;
        }


        public async Task Update(string productId, IProductRequestModel productModel)
        {
            var request = new RestRequest("/product/{id}", Method.POST);
            request.AddJsonBody(productModel);

            await Client.ExecuteAsync(request);
        }

        public async Task Delete(string productId)
        {
            var request = new RestRequest("/product/{id}", Method.DELETE);
            request.AddUrlSegment("id", productId);

            await Client.ExecuteAsync(request);
        }

        public async Task Activate(string productId)
        {
            var request = new RestRequest("/product/{id}/activate", Method.PATCH);
            request.AddUrlSegment("id", productId);

            await Client.ExecuteAsync(request);
        }

        public async Task Expire(string productId)
        {
            var request = new RestRequest("/product/{id}/expire", Method.PATCH);
            request.AddUrlSegment("id", productId);

            await Client.ExecuteAsync(request);
        }

        #endregion

        #region Consent Document

        public async Task<bool> AddConsentDocument(string productId, List<IConsentRequest> consentDocument)
        {
            var request = new RestRequest("/product/{productId}/documents/", Method.PUT);
            request.AddUrlSegment("productId", productId);
            request.AddJsonBody(consentDocument);

            return await Client.ExecuteAsync<bool>(request);
        }

        public async Task UpdateConsentDocument(string productId, string version, Format format, IConsentRequest consentDocument)
        {
            var request = new RestRequest("/{productId}/{version}/{format}/documents/", Method.PUT);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("version", version);
            request.AddUrlSegment("format", format.ToString());
            request.AddJsonBody(consentDocument);

            await Client.ExecuteAsync(request);
        }

        public async Task<TemplateManager.ITemplate> GetTemplateByProduct(string productId, string consentName, string version, Format format)
        {
            var request = new RestRequest("{productId}/{consentName}/{version}/{format}/template", Method.GET);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("consentName", consentName);
            request.AddUrlSegment("version", version);
            request.AddUrlSegment("format", format.ToString());
            return await Client.ExecuteAsync<TemplateManager.Template>(request);
        }

        public async Task DeleteConsentDocument(string productId, string consentName, string version, Format format)
        {
            var request = new RestRequest("{productId}/{consentName}/{version}/{format}/template", Method.DELETE);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("consentName", consentName);
            request.AddUrlSegment("version", version);
            request.AddUrlSegment("format", format.ToString());
            await Client.ExecuteAsync(request);
        }

        public async Task<List<IConsentDocument>> GetAllConsentDocument(string productId)
        {
            var request = new RestRequest("{productId}/all/consent-document", Method.GET);
            request.AddUrlSegment("productId", productId);
            var result = await Client.ExecuteAsync<List<ConsentDocument>>(request);
            return new List<IConsentDocument>(result);
        }

        #endregion

        #region Product Parameters

        public async Task<IProductParameters> AddProductParameters(string productId, IProductParametersRequest productParametersRequest)
        {
            var request = new RestRequest("{productId}/parameter", Method.POST);
            request.AddUrlSegment("productId", productId);
            request.AddJsonBody(productParametersRequest);
            return await Client.ExecuteAsync<ProductParameters>(request);
        }

        public async Task UpdateProductParameters(string productParameterId, IProductParametersRequest productParameterRequest)
        {
            var request = new RestRequest("{productParameterId}/parameter", Method.PUT);
            request.AddUrlSegment("productParameterId", productParameterId);
            request.AddJsonBody(productParameterRequest);
            await Client.ExecuteAsync(request);
        }

        public async Task DeleteProductParameters(string productParameterId)
        {
            var request = new RestRequest("{productParameterId}/parameter", Method.DELETE);
            request.AddUrlSegment("productParameterId", productParameterId);
            await Client.ExecuteAsync(request);
        }

        public async Task<IProductParameters> GetProductParameter(string productParameterId)
        {
            var request = new RestRequest("{productParameterId}/parameter", Method.GET);
            request.AddUrlSegment("productParameterId", productParameterId);
            return await Client.ExecuteAsync<ProductParameters>(request);
        }

        public async Task<List<IProductParameters>> GetProductParameterByGroupName(string productId, string productGroupName)
        {
            var request = new RestRequest("{productId}/{productGroupName}/parameter", Method.GET);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("productGroupName", productGroupName);
            var result = await Client.ExecuteAsync<List<ProductParameters>>(request);
            return new List<IProductParameters>(result);
        }

        public async Task<List<IProductParameters>> GetAllProductParameters(string productId)
        {
            var request = new RestRequest("{productId}/all/parameter", Method.GET);
            request.AddUrlSegment("productId", productId);
            var result = await Client.ExecuteAsync<List<ProductParameters>>(request);
            return new List<IProductParameters>(result);
        }

        public async Task<IProductParameterGroup> AddProductGroup(string productId, IProductGroupRequest productGroupRequest)
        {
            var request = new RestRequest("{productId}/group", Method.POST);
            request.AddUrlSegment("productId", productId);
            request.AddJsonBody(productGroupRequest);
            return await Client.ExecuteAsync<ProductParameterGroup>(request);
        }

        public async Task UpdateProductGroup(string productGroupId, IProductGroupRequest productGroupRequest)
        {
            var request = new RestRequest("{productGroupId}/group", Method.PUT);
            request.AddUrlSegment("productGroupId", productGroupId);
            request.AddJsonBody(productGroupRequest);
            await Client.ExecuteAsync(request);
        }

        public async Task DeleteProductGroup(string productGroupId)
        {
            var request = new RestRequest("{productGroupId}/group", Method.DELETE);
            request.AddUrlSegment("productGroupId", productGroupId);
            await Client.ExecuteAsync(request);
        }

        public async Task<IProductParameterGroup> GetProductGroup(string productGroupId)
        {
            var request = new RestRequest("{productGroupId}/group", Method.GET);
            request.AddUrlSegment("productGroupId", productGroupId);
            return await Client.ExecuteAsync<ProductParameterGroup>(request);
        }

        public async Task<List<IProductParameterGroup>> GetAllProductGroup(string productId)
        {
            var request = new RestRequest("{productId}/all/group", Method.GET);
            request.AddUrlSegment("productId", productId);
            var result = await Client.ExecuteAsync<List<ProductParameterGroup>>(request);
            return new List<IProductParameterGroup>(result);
        }


        #endregion

        public async Task<List<IWorkFlow>> GetAllStatusWorkFlow(string productId)
        {
            var request = new RestRequest("{productId}/all/work-flow", Method.GET);
            request.AddUrlSegment("productId", productId);
            var result = await Client.ExecuteAsync<List<WorkFlow>>(request);
            return new List<IWorkFlow>(result);
        }

        public async Task<IProduct> AddStatusWorkFlow(string productId, IWorkFlowRequest workFlowRequest)
        {
            var request = new RestRequest("{productId}/work-flow", Method.POST);
            request.AddUrlSegment("productId", productId);
            request.AddJsonBody(workFlowRequest);
            return await Client.ExecuteAsync<Product>(request);
        }

        public async Task<IProduct> UpdateStatusWorkFlow(string productId, string workFlowId, IWorkFlowRequest workFlowRequest)
        {
            var request = new RestRequest("{productId}/{workFlowId}/work-flow", Method.PUT);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("workFlowId", workFlowId);
            request.AddJsonBody(workFlowRequest);
            return await Client.ExecuteAsync<Product>(request);
        }

        public async Task DeleteStatusWorkFlow(string productId, string workFlowId)
        {
            var request = new RestRequest("{productId}/{workFlowId}/work-flow", Method.DELETE);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("workFlowId", workFlowId);
            await Client.ExecuteAsync(request);
        }

        public async Task<IWorkFlow> GetWorkFlowByName(string productId, string workFlowName)
        {
            var request = new RestRequest("{productId}/by/name/{workFlowName}", Method.GET);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("workFlowName", workFlowName);
            return await Client.ExecuteAsync<WorkFlow>(request);
        }

        public async Task<List<IProduct>> GetAll()
        {
            var request = new RestRequest("all/product", Method.GET);
            var result = await Client.ExecuteAsync<List<Product>>(request);
            return new List<IProduct>(result);
        }

        public async Task<IProductTemplate> GetProductTemplate(PortfolioType portfolioType)
        {
            var request = new RestRequest("template/{portfolioType}", Method.GET);
            request.AddUrlSegment("portfolioType", portfolioType.ToString());
            return await Client.ExecuteAsync<ProductTemplate>(request);
        }

        public async Task<IProductFeeParameters> AddProductFeeParameters(string productId, IProductFeeParametersRequest productFeeParametersRequest)
        {
            var request = new RestRequest("{productId}/feeparameter", Method.POST);
            request.AddUrlSegment("productId", productId);
            request.AddJsonBody(productFeeParametersRequest);
            return await Client.ExecuteAsync<ProductFeeParameters>(request);
        }

        public async Task UpdateProductFeeParameters(string productFeeParameterId, IProductFeeParametersRequest productFeeParametersRequest)
        {
            var request = new RestRequest("{productFeeParameterId}/feeparameter", Method.PUT);
            request.AddUrlSegment("productFeeParameterId", productFeeParameterId);
            request.AddJsonBody(productFeeParametersRequest);
            await Client.ExecuteAsync(request);
        }

        public async Task DeleteProductFeeParameters(string productFeeParameterId)
        {
            var request = new RestRequest("{productFeeParameterId}/feeparameter", Method.DELETE);
            request.AddUrlSegment("productFeeParameterId", productFeeParameterId);
            await Client.ExecuteAsync(request);
        }

        public async Task<IProductFeeParameters> GetProductFeeParameter(string productFeeParameterId)
        {
            var request = new RestRequest("{productFeeParameterId}/feeparameter", Method.GET);
            request.AddUrlSegment("productFeeParameterId", productFeeParameterId);
            return await Client.ExecuteAsync<ProductFeeParameters>(request);
        }

        public async Task<List<IProductFeeParameters>> GetAllProductFeeParameters(string productId)
        {
            var request = new RestRequest("{productId}/all/feeparameter", Method.GET);
            request.AddUrlSegment("productId", productId);
            var result = await Client.ExecuteAsync<List<ProductFeeParameters>>(request);
            return new List<IProductFeeParameters>(result);

        }

        public async Task<List<IProductFeeParameters>> GetProductFeeParameterByGroupName(string productId, string groupName)
        {
            var request = new RestRequest("{productId}/{groupName}/feeparameter", Method.GET);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("groupName", groupName);
            var result = await Client.ExecuteAsync<List<ProductFeeParameters>>(request);
            return new List<IProductFeeParameters>(result);
        }

        public async Task<IProductTemplate> AddTemplateDetail(IProductTemplateRequest requestParameter)
        {
            var request = new RestRequest("/template", Method.POST);
            request.AddJsonBody(requestParameter);
            return await Client.ExecuteAsync<ProductTemplate>(request);
        }

        public async Task AddPaymentHierarchy(string productId, List<PaymentHierarchy> paymentHierarchy)
        {
            var request = new RestRequest("{id}/paymenthierarchy", Method.PUT);
            request.AddJsonBody(paymentHierarchy);
            await Client.ExecuteAsync(request);
        }

        public async Task<List<IPaymentHierarchy>> GetPaymentHierarchy(string productId)
        {
            var request = new RestRequest("{id}/paymenthierarchy", Method.GET);
            request.AddUrlSegment("productId", productId);          
            var result = await Client.ExecuteAsync<List<PaymentHierarchy>>(request);
            return new List<IPaymentHierarchy>(result);
        }
    }
}
