﻿using LendFoundry.Security.Tokens;
using System;

#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif


namespace LendFoundry.ProductConfiguration.Client
{
    public static class ProductServiceExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddProductService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IProductServiceFactory>(p => new ProductServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IProductServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddProductService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IProductServiceFactory>(p => new ProductServiceFactory(p, uri));
            services.AddTransient(p => p.GetService<IProductServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddProductService(this IServiceCollection services)
        {
            services.AddTransient<IProductServiceFactory>(p => new ProductServiceFactory(p));
            services.AddTransient(p => p.GetService<IProductServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
