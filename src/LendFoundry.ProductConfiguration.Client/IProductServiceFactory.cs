﻿using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration.Client
{
    public interface IProductServiceFactory
    {
        IProductService Create(ITokenReader reader);
    }
}
