﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.ProductConfiguration
{
    public interface IPaymentHierarchy
    {
        string Name { get; set; }

        string Purpose { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPaymentElement, PaymentElement>))]
        List<IPaymentElement> Hierarchy { get; set; }
    }
}
