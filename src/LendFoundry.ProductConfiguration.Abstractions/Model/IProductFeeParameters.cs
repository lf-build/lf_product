﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.ProductConfiguration
{
    public interface IProductFeeParameters : IAggregate
    {
        string ProductId { get; set; }

        string FeeId { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        string ApplicableRule { get; set; }
        string GroupId { get; set; }
        string CalculationRule { get; set; }
        double FeeAmount { get; set; }
        AppliedOnType AppliedOnType { get; set; }
        bool IsAccrue { get; set; }
        string FeeType { get; set; }

        string FeePaymentFrequency { get; set; }

        List<string> ApplicableEvents { get; set; }

        bool AutoPay { get; set; }

        bool IsSystem { get; set; }
    }
}
