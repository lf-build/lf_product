﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public interface ICharter
    {
        int BankId { get; set; }
        Geographies SupportedGeographies { get; set; }
    }
}
