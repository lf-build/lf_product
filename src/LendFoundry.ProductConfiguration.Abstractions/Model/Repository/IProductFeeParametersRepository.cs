﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public interface IProductFeeParametersRepository : IRepository<IProductFeeParameters>
    {

        Task<IProductFeeParameters> GetFeeDetailsById(string feeId);

        Task<IProductFeeParameters> GetByName(string productId, string Name);

        Task<IProductFeeParameters> GetByFeeId(string productId, string feeId);

        Task<List<IProductFeeParameters>> GetAllByProductId(string productId);

        Task<List<IProductFeeParameters>> GetProductParameterDetailsByGroupId(string productGroupId);

       void AddOrUpdate(IProductFeeParameters productFeeParameters);
    }
}
