﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public interface IProductTemplateRepository : IRepository<IProductTemplate>
    {
        Task<IProductTemplate> GetProductTemplate(PortfolioType portfolioType);

        void AddOrUpdate(IProductTemplate productTemplate);
    }
}
