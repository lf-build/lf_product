﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public interface IProductParametersRepository : IRepository<IProductParameters>
    {

        Task<List<IProductParameters>> GetProductParameterDetailsByGroupId(string productGroupId);

        Task<IProductParameters> GetByName(string productId, string Name);

        Task<List<IProductParameters>> GetAllByProductId(string productId);
        Task<IProductParameters> GetByParameterId(string productId, string parameterId);
    }
}
