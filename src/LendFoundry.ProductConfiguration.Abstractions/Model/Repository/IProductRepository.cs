﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public interface IProductRepository : IRepository<IProduct>
    {
        Task<IProduct> GetProductDetails(string id);
        Task<List<IProduct>> GetAll();
        string AddProduct(IProduct product);

        Task<long> UpdateProduct(string productId, IProduct product);

        Task<long> ActivateProduct(string productId);

        Task<long> DeActivateProduct(string productId);

        Task<long> RemoveProduct(string productId);
        
        #region IConsentDocument 

        Task<long> AddConsentDocument(string productId, List<IConsentDocument> consentDocument);
        Task<long> UpdateConsentDocument(string productId, List<IConsentDocument> consentDocument);
        #endregion

        
    }
}
