﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public interface IProductGroupRepository : IRepository<IProductParameterGroup>
    {

        Task<IProductParameterGroup> GetProductDetails(string productGroupId);

        Task<IProductParameterGroup> GetByName(string productId, string Name);

        Task<List<IProductParameterGroup>> GetAllByProductId(string productId);

        Task<IProductParameterGroup> GetById(string productId, string groupId);

    }
}
