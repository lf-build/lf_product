﻿
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LendFoundry.ProductConfiguration
{
    public class Product : Aggregate, IProduct
    {
        public string ProductId { get; set; }
        public PortfolioType PortfolioType { get; set; }
        public ProductCategory ProductCategory { get; set; }
        public bool IsSecured { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FamilyId { get; set; }

        public ProductStatus Status { get; set; }

        public TimeBucket ValidFrom { get; set; }

        public TimeBucket ExpiresOn { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IConsentDocument, ConsentDocument>))]
        public List<IConsentDocument> Documents { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IWorkFlow, WorkFlow>))]
        public List<IWorkFlow> WorkFlow { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPaymentHierarchy, PaymentHierarchy>))]
        public List<IPaymentHierarchy> PaymentHierarchy { get; set; }

    }
}
