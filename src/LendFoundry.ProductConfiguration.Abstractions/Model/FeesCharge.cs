﻿
using System;

namespace LendFoundry.ProductConfiguration
{
    public class FeesCharge : IFeesCharge
    {
        public FeeType FeeType { get; set; }
        public string FeeDescription { get; set; }
        public Rule FeeAmount { get; set; }
        public Rule Applicability { get; set; }
        public bool ApplyInterest { get; set; }
        public bool Amortize { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset ExpiresOn { get; set; }
        public int ChartOfAccountMapping { get; set; }
        public PaymentElement PaymentElement{ get; set; }        
    }
}
