﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public interface IUsuryRules
    {
         string RulePurpose { get; set; }
        Geographies ApplicableGeography { get; set; }
         string Rule { get; set; }
         DateTimeOffset StartDate { get; set; }
         DateTimeOffset ExpiresOn { get; set; }
    }
}
