﻿using System;

namespace LendFoundry.ProductConfiguration
{
    public interface IRule
    {
        string Name { get; set; }
        string Description { get; set; }
    }
}
