﻿using System;

namespace LendFoundry.ProductConfiguration
{
    public class Rule : IRule
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}