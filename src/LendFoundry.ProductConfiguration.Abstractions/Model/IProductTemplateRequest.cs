﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
     public interface IProductTemplateRequest
    {
        PortfolioType PortfolioType { get; set; }
        object Template { get; set; }
    }
}
