﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public interface IDocument
    {
        string Name { get; set; }
        byte[] Stream { get; set; }
    }
}
