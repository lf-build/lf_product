﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.ProductConfiguration
{
    public class ProductGroupRequest : IProductGroupRequest
    {

        public string GroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Version { get; set; }

        public DateTimeOffset ValidFrom { get; set; }

        public DateTimeOffset ExpiresOn { get; set; }


    }
}
