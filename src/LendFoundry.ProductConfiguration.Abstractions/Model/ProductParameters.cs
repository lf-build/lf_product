﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.ProductConfiguration
{
    public class ProductParameters : Aggregate, IProductParameters
    {
        public string ProductId { get; set; }
        public string ParameterId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Purpose { get; set; }
        public string Value { get; set; }
        public string UnitValue { get; set; }
        public string ParameterType { get; set; }
        public string GroupId { get; set; }
        public string CalculationRuleId { get; set; }
        public string ApplicableRuleId { get; set; }
        public string LookUpForValue { get; set; }
        public string LookUpForUnit { get; set; }
        public bool IsSystem { get; set; }
       public List<string> ApplicableEvents { get; set; }
    }
}
