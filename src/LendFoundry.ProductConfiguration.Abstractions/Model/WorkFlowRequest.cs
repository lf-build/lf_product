﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public class WorkFlowRequest : IWorkFlowRequest
    {
        public string EntityType { get; set; }
        public string WorkFlowName { get; set; }
        public WorkFlowType WorkFlowType { get; set; }
        public string Description { get; set; }
        public DateTimeOffset ValidFrom { get; set; }
        public DateTimeOffset ExpiresOn { get; set; }
    }
}
