﻿
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LendFoundry.ProductConfiguration
{
    public interface IProduct : IProductBase, IAggregate
    {
        [JsonConverter(typeof(InterfaceListConverter<IConsentDocument, ConsentDocument>))]
        List<IConsentDocument> Documents { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IWorkFlow, WorkFlow>))]
        List<IWorkFlow> WorkFlow { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPaymentHierarchy, PaymentHierarchy>))]
        List<IPaymentHierarchy> PaymentHierarchy { get; set; }
    }
}
