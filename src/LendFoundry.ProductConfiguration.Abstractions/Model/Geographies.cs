﻿
using System.Collections.Generic;

namespace LendFoundry.ProductConfiguration
{
    public class Geographies : IGeographies
    {
        public Country Country { get; set; }
        public List<State> IncludedStates { get; set; }
        public List<State> ExcludedStates { get; set; }
    }
}
