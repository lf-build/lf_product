﻿using System;


namespace LendFoundry.ProductConfiguration
{
    public class FundingSource : IFundingSource
    {
        public string Name { get; set; }
    }
}
