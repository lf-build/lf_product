﻿
namespace LendFoundry.ProductConfiguration
{
    public class Charter : ICharter
    {
        public int BankId { get; set; }
        public Geographies SupportedGeographies { get; set; }
    }
}
