﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public class ParameterValue
    {
        public string Name { get; set; }
        public string ParameterId { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public List<string> ApplicableEvents { get; set; }
        public string UnitValue { get; set; }
        public string ParameterType { get; set; }
        public string GroupName { get; set; }
    }
}
