﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public interface IInterest
    {
        InterestType InterestType { get; set; }
        FrequencyType CompoundingPeriod { get; set; }
        TermDuration TermDuration { get; set; }
        List<LoanTerm> SupportedLoanTerms { get; set; }
        FrequencyType PaymentFrequency { get; set; }
        PaymentType PaymentType { get; set; }
        PaymentTime PaymentTime { get; set; }
        AmortizationCalendar AmortizationCalendar { get; set; }
        string FirstPaymentDate { get; set; }
        bool SupportDeferredInterest { get; set; }
        string DaysToStartInterestAccrual { get; set; }
    }
}
