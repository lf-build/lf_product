﻿
namespace LendFoundry.ProductConfiguration
{
    public class LoanTerm : ILoanTerm
    {
        public FrequencyType Frequency { get; set; }
        public int Term { get; set; }
    }
}
