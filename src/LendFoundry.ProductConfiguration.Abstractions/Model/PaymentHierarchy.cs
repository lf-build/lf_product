﻿
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.ProductConfiguration
{
    public class PaymentHierarchy : IPaymentHierarchy
    {

        public string Name { get; set; }

        public string Purpose { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPaymentElement, PaymentElement>))]
        public List<IPaymentElement> Hierarchy { get; set; }
    }
}
