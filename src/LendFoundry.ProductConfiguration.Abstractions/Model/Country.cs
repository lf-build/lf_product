﻿
namespace LendFoundry.ProductConfiguration
{
    public class Country  : ICountry
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
