﻿using System;

namespace LendFoundry.ProductConfiguration
{
    public interface IWorkFlowRequest
    {
         string EntityType { get; set; }
        string WorkFlowName { get; set; }
        WorkFlowType WorkFlowType { get; set; }
        string Description { get; set; }
        DateTimeOffset ValidFrom { get; set; }
        DateTimeOffset ExpiresOn { get; set; }
    }
}