﻿namespace LendFoundry.ProductConfiguration
{
    public enum WorkFlowType
    {
        ApplicationApproval,
        DrawdownApproval,
        Extension,
        DefaultApproval,
        LoanServicing,
        RenewalApplicationApproval
    }
}