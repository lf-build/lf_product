﻿using LendFoundry.Foundation.Date;
using LendFoundry.TemplateManager;
using System.Collections.Generic;

namespace LendFoundry.ProductConfiguration
{
    public class ConsentDocument : IConsentDocument
    {       
       
        public string ConsentId { get; set; }
        public string ConsentName { get; set; }

        public string ConsentCategory { get; set; }
        public string ConsentPurpose { get; set; }
        public string TemplateId { get; set; }
        public string DocumentDisplayName { get; set; }
        public TimeBucket ValidFrom { get; set; }
        public TimeBucket ExpiresOn { get; set; }        
        public Format Format { get; set; }
        public string Version { get; set; }
        public List<string> Tags { get; set; }
    }
}
