﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public interface ICountry
    {
        string Name { get; set; }
        string Code { get; set; }
    }
}
