﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public class ProductRequestModel : IProductRequestModel
    {
       public string ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FamilyId { get; set; }
        public PortfolioType ProductType { get; set; }
        public ProductCategory ProductCategory { get; set; }
        public bool IsSecured { get; set; }
        public DateTimeOffset ValidFrom { get; set; }
        public DateTimeOffset ExpiresOn { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IConsentRequest, ConsentRequest>))]
        public List<IConsentRequest> Documents { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IWorkFlowRequest, WorkFlowRequest>))]
        public List<IWorkFlowRequest> WorkFlow { get; set; }

    }
}
