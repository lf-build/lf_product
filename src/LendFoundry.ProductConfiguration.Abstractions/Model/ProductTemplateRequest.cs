﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public class ProductTemplateRequest : IProductTemplateRequest
    {
        public PortfolioType PortfolioType { get; set; }
        public object Template { get; set; }
    }
}
