﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public interface ILoanTerm
    {
        FrequencyType Frequency { get; set; }
        int Term { get; set; }
    }
}
