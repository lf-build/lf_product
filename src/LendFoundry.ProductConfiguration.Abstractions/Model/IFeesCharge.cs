﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public interface IFeesCharge 
    {
        FeeType FeeType { get; set; }
        string FeeDescription { get; set; }
        Rule FeeAmount { get; set; }
        Rule Applicability { get; set; }
        bool ApplyInterest { get; set; }
        bool Amortize { get; set; }
        DateTimeOffset StartDate { get; set; }
        DateTimeOffset ExpiresOn { get; set; }
        int ChartOfAccountMapping { get; set; }

        PaymentElement PaymentElement { get; set; }
    }
}
