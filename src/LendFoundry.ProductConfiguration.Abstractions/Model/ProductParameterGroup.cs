﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.ProductConfiguration
{
    public class ProductParameterGroup : Aggregate, IProductParameterGroup
    {
        public string GroupId { get; set; }
        public string ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Version { get; set; }
        public TimeBucket ValidFrom { get; set; }
        public TimeBucket ExpiresOn { get; set; }
        public TimeBucket CreatedOn { get; set; }
        public TimeBucket UpdatedOn { get; set; }     
       
    }
}
