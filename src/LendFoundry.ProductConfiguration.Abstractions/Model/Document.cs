﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public class Document : IDocument
    {
        public string Name { get; set; }
        public byte[] Stream { get; set; }
    }
}
