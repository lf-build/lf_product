﻿namespace LendFoundry.ProductConfiguration
{
    public interface IFundingSource
    {
        string Name { get; set; }
    }
}
