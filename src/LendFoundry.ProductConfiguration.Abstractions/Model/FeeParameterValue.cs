﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public class FeeParameterValue
    {
        public string Name { get; set; }
        public string FeeId { get; set; }
        public string FeeType { get; set; }
        public string Description { get; set; }
        public List<string> ApplicableEvents { get; set; }
        public AppliedOnType AppliedOnType { get; set; }
        public double FeeAmount { get; set; }
        public string GroupName { get; set; }
        public string FeePaymentFrequency { get; set; }
        public bool IsAccrue { get; set; }
        public bool AutoPay { get; set; }

    }
}
