﻿
namespace LendFoundry.ProductConfiguration
{
    public class State : IState
    {
        public string CountryCode { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
