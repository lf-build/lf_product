﻿using System;

namespace LendFoundry.ProductConfiguration
{
    public class UsuryRules : IUsuryRules
    {
        public string RulePurpose { get; set; }
        public Geographies ApplicableGeography { get; set; }
        public string Rule { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset ExpiresOn { get; set; }
    }
}
