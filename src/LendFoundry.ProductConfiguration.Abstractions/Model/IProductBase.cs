﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public interface IProductBase
    {
        string ProductId { get; set; }
        string FamilyId { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        ProductStatus Status { get; set; }
        TimeBucket ValidFrom { get; set; }
        TimeBucket ExpiresOn { get; set; }
        PortfolioType PortfolioType { get; set; }
        ProductCategory ProductCategory { get; set; }
        bool IsSecured { get; set; }
    }
}
