﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.ProductConfiguration
{
    public interface IProductGroupRequest 
    {
        string GroupId { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        string Version { get; set; }

        DateTimeOffset ValidFrom { get; set; }

        DateTimeOffset ExpiresOn { get; set; }
    }
}
