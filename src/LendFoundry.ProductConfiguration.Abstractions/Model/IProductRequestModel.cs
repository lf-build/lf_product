﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace LendFoundry.ProductConfiguration
{
    public interface IProductRequestModel
    {
        string ProductId { get; set; }
        string FamilyId { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        PortfolioType ProductType { get; set; }
        ProductCategory ProductCategory { get; set; }
        bool IsSecured { get; set; }
        DateTimeOffset ValidFrom { get; set; }
        DateTimeOffset ExpiresOn { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IConsentRequest, ConsentRequest>))]
        List<IConsentRequest> Documents { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IWorkFlowRequest, WorkFlowRequest>))]
         List<IWorkFlowRequest> WorkFlow { get; set; }
    }
}
