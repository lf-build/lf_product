﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.ProductConfiguration
{
    public interface IProductParametersRequest
    {
        string ParameterId { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        string Value { get; set; }
        string Purpose { get; set; }
        string ParameterType { get; set; }
        string GroupId { get; set; }
        string CalculationRuleId { get; set; }
        string UnitValue { get; set; }
        string ApplicableRuleId { get; set; }
        string LookUpValue { get; set; }
        string LookUpValueType { get; set; }
        bool IsSystem { get; set; }

        List<string> ApplicableEvents { get; set; }
    }
}
