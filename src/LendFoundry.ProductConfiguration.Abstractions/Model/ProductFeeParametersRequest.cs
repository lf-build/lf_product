﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.ProductConfiguration
{
    public class ProductFeeParametersRequest : IProductFeeParametersRequest
    {

        public string Name { get; set; }

        public string FeeId { get; set; }
        public string Description { get; set; }
        public string ApplicableRule { get; set; }
        public string CalculationRule { get; set; }

        public string GroupId { get; set; }
        public double FeeAmount { get; set; }

        public AppliedOnType AppliedOnType { get; set; }
        public string FeeType { get; set; }
        public string FrequencyType { get; set; }

        public bool IsAccrue { get; set; }
        public bool AutoPay { get; set; }

        public List<string> Events { get; set; }

        public bool IsSystem { get; set; }

    }
}
