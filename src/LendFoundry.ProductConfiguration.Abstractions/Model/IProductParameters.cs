﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.ProductConfiguration
{
    public interface IProductParameters : IAggregate
    {

        string ProductId { get; set; }
        string ParameterId { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        string Purpose { get; set; }
        string Value { get; set; }
        string UnitValue { get; set; }
        string ParameterType { get; set; }
        string GroupId { get; set; }
        string CalculationRuleId { get; set; }
        string ApplicableRuleId { get; set; }
        string LookUpForValue { get; set; }
        string LookUpForUnit { get; set; }
        bool IsSystem { get; set; }
        List<string> ApplicableEvents { get; set; }
    }
}
