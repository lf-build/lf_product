﻿using System.Collections.Generic;

namespace LendFoundry.ProductConfiguration
{
    public interface IGeographies
    {
        Country Country { get; set; }
        List<State> IncludedStates { get; set; }
        List<State> ExcludedStates { get; set; }
    }
}
