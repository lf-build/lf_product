﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.ProductConfiguration
{
    public interface IProductTemplate : IAggregate
    {      
         PortfolioType PortfolioType { get; set; }      
         object Template { get; set; }
    }
}