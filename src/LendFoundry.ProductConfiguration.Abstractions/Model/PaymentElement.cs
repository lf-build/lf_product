﻿
namespace LendFoundry.ProductConfiguration
{
    public class PaymentElement : IPaymentElement
    {
        public int Priority { get; set; }
        public string Element { get; set; }
    }
}
