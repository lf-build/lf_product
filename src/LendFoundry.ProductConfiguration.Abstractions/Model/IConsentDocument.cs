﻿using LendFoundry.Foundation.Date;
using LendFoundry.TemplateManager;
using System;
using System.Collections.Generic;

namespace LendFoundry.ProductConfiguration
{
    public interface IConsentDocument
    {
        string ConsentId { get; set; }
        string ConsentName { get; set; }
        string ConsentCategory { get; set; }
        string ConsentPurpose { get; set; }      
        string DocumentDisplayName { get; set; }
        TimeBucket ValidFrom { get; set; }
        TimeBucket ExpiresOn { get; set; }
        string TemplateId { get; set; }
        string Version { get; set; }
        Format Format { get; set; }
        List<string> Tags { get; set; }
    }
}
