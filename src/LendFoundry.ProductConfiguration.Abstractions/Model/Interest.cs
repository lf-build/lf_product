﻿using System.Collections.Generic;

namespace LendFoundry.ProductConfiguration
{
    public class Interest : IInterest
    {
        public InterestType InterestType { get; set; }
        public FrequencyType CompoundingPeriod { get; set; }

        public TermDuration TermDuration { get; set; }
        public List<LoanTerm> SupportedLoanTerms { get; set; }
        public FrequencyType PaymentFrequency { get; set; }
        public PaymentType PaymentType { get; set; }
        public PaymentTime PaymentTime { get; set; }
        public AmortizationCalendar AmortizationCalendar { get; set; }
        public string FirstPaymentDate { get; set; }
        public bool SupportDeferredInterest { get; set; }
        public string DaysToStartInterestAccrual { get; set; }   }
}
