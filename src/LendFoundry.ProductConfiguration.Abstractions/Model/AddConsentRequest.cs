﻿
using LendFoundry.TemplateManager;
using System;
using System.Collections.Generic;

namespace LendFoundry.ProductConfiguration
{
    public class ConsentRequest : IConsentRequest
    {
        public string ConsentName { get; set; }
        public string ConsentCategory { get; set; }
        public string ConsentPurpose { get; set; }   
        public string DocumentDisplayName { get; set; }
        public DateTimeOffset ValidFrom { get; set; }
        public DateTimeOffset ExpiresOn { get; set; }

        public string Title { get; set; }
        public Format Format { get; set; }
        public string Body { get; set; }
        public string Version { get; set; }
        public Dictionary<string, object> Properties { get; set; }
       public List<string> Tags { get; set; }
    }
}
