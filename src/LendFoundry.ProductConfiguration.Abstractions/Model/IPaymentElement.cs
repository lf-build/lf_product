﻿namespace LendFoundry.ProductConfiguration
{
    public interface IPaymentElement
    {
        int Priority { get; set; }
        string Element { get; set; }
    }
}
