﻿using LendFoundry.TemplateManager;
using System;
using System.Collections.Generic;

namespace LendFoundry.ProductConfiguration
{
    public interface IConsentRequest
    {
       
        string ConsentName { get; set; }
        string ConsentCategory { get; set; }
        string ConsentPurpose { get; set; }      
        string DocumentDisplayName { get; set; }
        DateTimeOffset ValidFrom { get; set; }
        DateTimeOffset ExpiresOn { get; set; }

        string Title { get; set; }
        Format Format { get; set; }
        string Body { get; set; }
        string Version { get; set; }
        Dictionary<string, object> Properties { get; set; }
        List<string> Tags { get; set; }
    }
}
