﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.ProductConfiguration
{
    public class ProductTemplate : Aggregate, IProductTemplate
    {
        public ProductTemplate(IProductTemplateRequest request)
        {
            PortfolioType = request.PortfolioType;
            Template = request.Template;
        }

        public PortfolioType PortfolioType { get; set; }     
        public object Template { get; set; }
    }
}
