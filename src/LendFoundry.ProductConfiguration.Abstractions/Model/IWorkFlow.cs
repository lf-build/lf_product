﻿using LendFoundry.Foundation.Date;
using System;

namespace LendFoundry.ProductConfiguration
{
    public interface IWorkFlow
    {
        string Id { get; set; }
        string EntityType { get; set; }
        string WorkFlowName { get; set; }
        WorkFlowType WorkFlowType { get; set; }
        string Description { get; set; }
        TimeBucket ValidFrom { get; set; }
        TimeBucket ExpiresOn { get; set; }
    }
}