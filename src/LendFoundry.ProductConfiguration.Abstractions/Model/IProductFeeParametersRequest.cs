﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.ProductConfiguration
{
    public interface IProductFeeParametersRequest
    {

        string Name { get; set; }

        string FeeId { get; set; }
        string Description { get; set; }
        string ApplicableRule { get; set; }

        string GroupId { get; set; }
        string CalculationRule { get; set; }
        double FeeAmount { get; set; }

        AppliedOnType AppliedOnType { get; set; }
        string FeeType { get; set; }
        string FrequencyType { get; set; }

        bool IsAccrue { get; set; }

        bool AutoPay { get; set; }

        List<string> Events { get; set; }

        bool IsSystem { get; set; }

    }
}
