﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.ProductConfiguration
{
    public interface IProductParameterGroup : IAggregate
    {
        string GroupId { get; set; }
        string ProductId { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        string Version { get; set; }
        TimeBucket ValidFrom { get; set; }
        TimeBucket ExpiresOn { get; set; }
        TimeBucket CreatedOn { get; set; }
        TimeBucket UpdatedOn { get; set; }
    }
}
