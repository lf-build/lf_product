﻿using LendFoundry.Foundation.Client;

namespace LendFoundry.ProductConfiguration
{
    public  interface IProductConfiguration : IDependencyConfiguration
    {
        string ConnectionString { get; set; }
    }
}
