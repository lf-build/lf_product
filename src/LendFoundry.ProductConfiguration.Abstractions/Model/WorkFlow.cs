﻿using LendFoundry.Foundation.Date;
using System;


namespace LendFoundry.ProductConfiguration
{
    public class WorkFlow : IWorkFlow
    {     
        public string Id { get; set; }
        public string EntityType { get; set; }
        public string WorkFlowName { get; set; }
        public WorkFlowType WorkFlowType { get; set; }
        public string Description { get; set; }
        public TimeBucket ValidFrom { get; set; }
        public TimeBucket ExpiresOn { get; set; }
    }
}
