﻿using System;

namespace LendFoundry.ProductConfiguration
{
    public class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "product";
    }
}
