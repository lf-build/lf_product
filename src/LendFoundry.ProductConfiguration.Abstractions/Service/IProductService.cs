﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.TemplateManager;


namespace LendFoundry.ProductConfiguration
{
    public interface IProductService
    {
        Task<IProduct> Get(string productId);
        Task<List<IProduct>> GetAll();
        Task<string> Add(IProductRequestModel productModel);

        Task Update(string productId, IProductRequestModel productModel);

        Task Delete(string productId);

        Task Activate(string productId);

        Task Expire(string productId);   

        Task<bool> AddConsentDocument(string productId, List<IConsentRequest> consentDocument);

        Task UpdateConsentDocument(string productId,string version, Format format, IConsentRequest consentDocument);

        Task DeleteConsentDocument(string productId, string consentName, string version, Format format);

        Task<List<IConsentDocument>> GetAllConsentDocument(string productId);

        Task<IProductParameters> AddProductParameters(string productId, IProductParametersRequest productParametersRequest);

        Task UpdateProductParameters(string productParameterId, IProductParametersRequest productParameterRequest);

        Task DeleteProductParameters(string productParameterId);

        Task<IProductParameters> GetProductParameter(string productParameterId);

        Task<List<IProductParameters>> GetAllProductParameters(string productId);

        Task<IProductParameterGroup> AddProductGroup(string productId, IProductGroupRequest productGroupRequest);

        Task UpdateProductGroup(string productGroupId, IProductGroupRequest productGroupRequest);

        Task DeleteProductGroup(string productGroupId);

        Task<IProductParameterGroup> GetProductGroup(string productGroupId);

        Task<List<IProductParameterGroup>> GetAllProductGroup(string productId);

        Task<ITemplate> GetTemplateByProduct(string productId, string consentName, string version, Format format);

        Task<List<IWorkFlow>> GetAllStatusWorkFlow(string productId);

        Task<IProduct> AddStatusWorkFlow(string productId, IWorkFlowRequest workFlowRequest);

        Task<IProduct> UpdateStatusWorkFlow(string productId, string workFlowId, IWorkFlowRequest workFlowRequest);

        Task DeleteStatusWorkFlow(string productId, string workFlowId);

        Task<IWorkFlow> GetWorkFlowByName(string productId, string workFlowName);

        Task<List<IProductParameters>> GetProductParameterByGroupName(string productId, string productGroupName);
        Task<IProductTemplate> GetProductTemplate(PortfolioType portfolioType);

        Task<IProductFeeParameters> AddProductFeeParameters(string productId, IProductFeeParametersRequest productFeeParametersRequest);

        Task UpdateProductFeeParameters(string productFeeParameterId, IProductFeeParametersRequest productFeeParametersRequest);
        Task DeleteProductFeeParameters(string productFeeParameterId);

        Task<IProductFeeParameters> GetProductFeeParameter(string productFeeParameterId);

        Task<List<IProductFeeParameters>> GetAllProductFeeParameters(string productId);

        Task<List<IProductFeeParameters>> GetProductFeeParameterByGroupName(string productId, string groupName);

        Task<IProductTemplate> AddTemplateDetail(IProductTemplateRequest request);

        Task AddPaymentHierarchy(string productId, List<PaymentHierarchy> paymentHierarchy);

        Task<List<IPaymentHierarchy>> GetPaymentHierarchy(string productId);
    }
}
