﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration.Events
{
    public class ProductExpired : IProductEvents
    {
        public string ProductId
        {
            get; set;
        }

        public IProduct Product
        {
            get; set;
        }
    }
}
