﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration.Events
{
    public interface IProductEvents
    {
        string ProductId { get; set; }

        IProduct Product { get; set; }
    }
}
