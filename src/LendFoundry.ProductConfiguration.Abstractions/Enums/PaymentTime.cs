﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public enum PaymentTime
    {
        EndOfPeriod = 0,
        BeginningOfPeriod = 1
    }
}
