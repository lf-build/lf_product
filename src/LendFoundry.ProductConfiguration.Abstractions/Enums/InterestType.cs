﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public enum InterestType
    {
        Fixed = 0,
        Variable = 1
    }
}
