﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public enum PortfolioType
    {
        Installment,
        Mortgage,
        LineOfCredit,
        Open,
        Revolving,
        MCA,
        MCALOC,
        SCF
    }
}
