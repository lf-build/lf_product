﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public enum FrequencyType
    {
        Annual = 0,
        SemiAnnual = 1,
        Quarterly = 2,
        BiMonthly = 3,
        Monthly = 4,
        SemiMonthly = 5,
        BiWeekly = 6,
        Weekly = 7,
        Daily = 8
    }
}
