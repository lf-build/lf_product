﻿
namespace LendFoundry.ProductConfiguration
{
    public enum ProductCategory
    {
        Individual,
        Business
    }
}
