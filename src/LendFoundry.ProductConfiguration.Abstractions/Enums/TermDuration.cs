﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public enum TermDuration
    {
        Week = 0,
        Month = 1,
        Year = 2
    }
}
