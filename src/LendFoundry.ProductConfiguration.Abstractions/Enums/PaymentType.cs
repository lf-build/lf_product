﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public enum PaymentType
    {
        Installment = 0,
        OneTime = 1,
    }
}
