﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public enum ProductStatus
    {      
        Active = 1,
        DeActive = 2     
    }
}
