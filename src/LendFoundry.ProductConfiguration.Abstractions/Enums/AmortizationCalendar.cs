﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration
{
    public enum AmortizationCalendar
    {
        Year360Days = 0,
        Year365Days = 1,
        Year366Days = 2
    }
}
