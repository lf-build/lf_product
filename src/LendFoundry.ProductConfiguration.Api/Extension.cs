﻿using System.Collections.Generic;


namespace LendFoundry.ProductConfiguration.Api
{
    internal static class Extension
    {
        private static Geographies GetGeographies(Geographies item)
        {
            var SupportedGeographies = new Geographies();
            var ExcludedStates = new List<State>();
            var IncludedStates = new List<State>();


            if (item.Country != null)
            {
                var Country = new Country()
                {
                    Code = item.Country.Code,
                    Name = item.Country.Name
                };

                SupportedGeographies.Country = Country;
            }
            if (item.ExcludedStates != null && item.ExcludedStates.Count > 0)
            {
                foreach (var exState in item.ExcludedStates)
                {
                    ExcludedStates.Add(new State()
                    {
                        Code = exState.Code,
                        Name = exState.Name,
                        CountryCode = exState.CountryCode
                    });
                }

                SupportedGeographies.ExcludedStates = ExcludedStates;
            }
            if (item.IncludedStates != null && item.IncludedStates.Count > 0)
            {
                foreach (var inState in item.IncludedStates)
                {
                    IncludedStates.Add(new State()
                    {
                        Code = inState.Code,
                        Name = inState.Name,
                        CountryCode = inState.CountryCode
                    });
                }
                SupportedGeographies.IncludedStates = IncludedStates;
            }

            return SupportedGeographies;
        }

        private static List<LoanTerm> GetSupportedLoanTerms(List<LoanTerm> SupportedLoanTerms)
        {
            var lstSupportedLoanTerms = new List<LoanTerm>();
            if (SupportedLoanTerms != null && SupportedLoanTerms.Count > 0)
            {
                foreach (var item in SupportedLoanTerms)
                {
                    lstSupportedLoanTerms.Add(new LoanTerm()
                    {
                        Term = item.Term,
                        Frequency = item.Frequency
                    });
                }
            }

            return lstSupportedLoanTerms;
        }
        public static Product ToProduct(this ProductRequestModel vm)
        {


            #region Consent Documents
            var lstConsentDocs = new List<IConsentDocument>();

            if (vm.Documents != null && vm.Documents.Count > 0)
            {
                foreach (var item in vm.Documents)
                {
                    var objDoc = new ConsentDocument();
                    objDoc.ConsentPurpose = item.ConsentPurpose;
                    objDoc.ConsentName = item.ConsentName;
                    objDoc.DocumentDisplayName = item.DocumentDisplayName;
                    objDoc.ExpiresOn = new Foundation.Date.TimeBucket(item.ExpiresOn);
                    objDoc.ValidFrom = new Foundation.Date.TimeBucket(item.ValidFrom);
                    lstConsentDocs.Add(objDoc);
                }
            }
            #endregion

            return new Product()
            {
                FamilyId = vm.FamilyId,
                Name = vm.Name,
                Description = vm.Description,
                Documents = lstConsentDocs,

            };
        }

        public static Charter ToCharter(this Charter vm)
        {
            return new Charter()
            {
                BankId = vm.BankId,
                SupportedGeographies = GetGeographies(vm.SupportedGeographies)
            };
        }

        public static FundingSource ToFundingSource(this FundingSource vm)
        {
            return new FundingSource()
            {
                Name = vm.Name
            };
        }

      

        public static Interest ToInterest(this Interest vm)
        {
            return new Interest()
            {
                AmortizationCalendar = vm.AmortizationCalendar,
                CompoundingPeriod = vm.CompoundingPeriod,
                DaysToStartInterestAccrual = vm.DaysToStartInterestAccrual,
                FirstPaymentDate = vm.FirstPaymentDate,
                InterestType = vm.InterestType,
                PaymentFrequency = vm.PaymentFrequency,
                PaymentTime = vm.PaymentTime,
                PaymentType = vm.PaymentType,
                SupportDeferredInterest = vm.SupportDeferredInterest,
                SupportedLoanTerms = GetSupportedLoanTerms(vm.SupportedLoanTerms),
                TermDuration = vm.TermDuration
            };
        }
    }
}
