﻿using System;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

using LendFoundry.Foundation.Date;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.TemplateManager;
using System.Net;

namespace LendFoundry.ProductConfiguration.Api.Controllers
{
    /// <summary>
    /// ProductController class
    /// </summary>
    /// <seealso cref="LendFoundry.Foundation.Services.ExtendedController" />
    /// <seealso cref="ExtendedController" />
    [Route("/")]
    public class ProductController : ExtendedController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProductController"/> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="tenantService">The tenant service.</param>
        /// <param name="tenantTime">The tenant time.</param>
        /// <param name="productService">The product service.</param>
        /// <exception cref="ArgumentNullException">
        /// logger
        /// or
        /// tenantService
        /// </exception>
        public ProductController(ILogger logger,
            ITenantService tenantService,
            ITenantTime tenantTime,
            IProductService productService):base(logger)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            if (tenantService == null) throw new ArgumentNullException(nameof(tenantService));


            TenantService = tenantService;
            ProductService = productService;
        }

        #region Variables

        private ITenantService TenantService { get; }

        private IProductService ProductService { get; }

        #endregion

        #region PRODUCT GET, ADD, UPDATE, DELETE, ACTIVATE and EXPIRE

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        [HttpGet("all/product")]
        [ProducesResponseType(typeof(List<IProduct>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAll()
        {
            return await ExecuteAsync(async () => Ok(await ProductService.GetAll()));

        }

        /// <summary>
        /// Gets the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("/product/{id}")]
        [ProducesResponseType(typeof(IProduct), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> Get(string id)
        {
            return await ExecuteAsync(async () => Ok(await ProductService.Get(id)));

        }

        /// <summary>
        /// Adds the product.
        /// </summary>
        /// <param name="requestModel">The request model.</param>
        /// <returns></returns>
        [HttpPost("/product")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddProduct([FromBody]ProductRequestModel requestModel)
        {
            return await ExecuteAsync(async () => Ok(await ProductService.Add(requestModel)));
        }

        /// <summary>
        /// Adds the template detail.
        /// </summary>
        /// <param name="requestModel">The request model.</param>
        /// <returns></returns>
        [HttpPost("/template")]
        [ProducesResponseType(typeof(IProductTemplate), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddTemplateDetail([FromBody]ProductTemplateRequest requestModel)
        {
            return await ExecuteAsync(async () => Ok(await ProductService.AddTemplateDetail(requestModel)));
        }

        /// <summary>
        /// Adds the payment hierarchy.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="paymentHierarchy">The payment hierarchy.</param>
        /// <returns></returns>
        [HttpPut("{id}/paymenthierarchy")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddPaymentHierarchy(string id, [FromBody]List<PaymentHierarchy> paymentHierarchy)
        {
            return await ExecuteAsync(async () =>
            {
                await ProductService.AddPaymentHierarchy(id, paymentHierarchy);
                return Ok();
            });

        }

        /// <summary>
        /// Gets the payment hierarchy.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}/paymenthierarchy")]
        [ProducesResponseType(typeof(List<IPaymentHierarchy>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetPaymentHierarchy(string id)
        {
            return await ExecuteAsync(async () => Ok(await ProductService.GetPaymentHierarchy(id)));
        }

        /// <summary>
        /// Updates the product.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="requestModel">The request model.</param>
        /// <returns></returns>
        [HttpPut("/product/{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateProduct(string id, [FromBody]ProductRequestModel requestModel)
        {
            return await ExecuteAsync(async () =>
            {
                await ProductService.Update(id, requestModel);
                return Ok();
            });
        }

        /// <summary>
        /// Deletes the product.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete("/product/{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeleteProduct(string id)
        {
            return await ExecuteAsync(async () =>
            {
                await ProductService.Delete(id);
                return Ok();
            });
        }

        /// <summary>
        /// Activates the product.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPut("/product/{id}/activate")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> ActivateProduct(string id)
        {
            return await ExecuteAsync(async () =>
            {
                await ProductService.Activate(id);
                return Ok();
            });
        }

        /// <summary>
        /// Expires the product.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPut("/product/{id}/expire")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> ExpireProduct(string id)
        {
            return await ExecuteAsync(async () =>
            {
                await ProductService.Expire(id);
                return Ok();
            });
        }

        #endregion

        #region Add Consent Document

        /// <summary>
        /// Adds the consent document.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost("/product/{productId}/template")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddConsentDocument(string productId, [FromBody] List<ConsentRequest> model)
        {
            return await ExecuteAsync(async () =>
            {
                await ProductService.AddConsentDocument(productId, model.ToList<IConsentRequest>());
                return Ok();
            });
        }

        /// <summary>
        /// Updates the consent document.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="version">The version.</param>
        /// <param name="format">The format.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPut("/{productId}/{version}/{format}/template")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateConsentDocument(string productId, string version, Format format, [FromBody] ConsentRequest model)
        {
            return await ExecuteAsync(async () =>
            {
                await ProductService.UpdateConsentDocument(productId, version, format, model);
                return Ok();
            });
        }

        /// <summary>
        /// Deletes the consent document.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="consentName">Name of the consent.</param>
        /// <param name="version">The version.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [HttpDelete("{productId}/{consentName}/{version}/{format}/template")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeleteConsentDocument(string productId, string consentName, string version, Format format)
        {
            return await ExecuteAsync(async () => { await ProductService.DeleteConsentDocument(productId, consentName, version, format); return new NoContentResult(); });
        }

        /// <summary>
        /// Gets the template by product.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="consentName">Name of the consent.</param>
        /// <param name="version">The version.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [HttpGet("{productId}/{consentName}/{version}/{format}/template")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetTemplateByProduct(string productId, string consentName, string version, Format format)
        {
            consentName = WebUtility.UrlDecode(consentName);
            version = WebUtility.UrlDecode(version);
            return await ExecuteAsync(async () => Ok(await ProductService.GetTemplateByProduct(productId, consentName, version, format)));

        }

        /// <summary>
        /// Gets all consent document.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <returns></returns>
        [HttpGet("{productId}/all/consent-document")]
        [ProducesResponseType(typeof(List<IConsentDocument>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAllConsentDocument(string productId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductService.GetAllConsentDocument(productId));
            });
        }

        #endregion

        #region StatusWorkFlow

        /// <summary>
        /// Adds the status work flow.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="workFlowRequest">The work flow request.</param>
        /// <returns></returns>
        [HttpPost("{productId}/work-flow")]
        [ProducesResponseType(typeof(IProduct), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddStatusWorkFlow(string productId, [FromBody]WorkFlowRequest workFlowRequest)
        {
            return await ExecuteAsync(async () => Ok(await ProductService.AddStatusWorkFlow(productId, workFlowRequest)));
        }

        /// <summary>
        /// Updates the status work flow.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="workFlowId">The work flow identifier.</param>
        /// <param name="workFlowRequest">The work flow request.</param>
        /// <returns></returns>
        [HttpPut("{productId}/{workFlowId}/work-flow")]
        [ProducesResponseType(typeof(IProduct), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateStatusWorkFlow(string productId, string workFlowId, [FromBody]WorkFlowRequest workFlowRequest)
        {
            return await ExecuteAsync(async () => { await ProductService.UpdateStatusWorkFlow(productId, workFlowId, workFlowRequest); return new NoContentResult(); });
        }

        /// <summary>
        /// Deletes the status work flow.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="workFlowId">The work flow identifier.</param>
        /// <returns></returns>
        [HttpDelete("{productId}/{workFlowId}/work-flow")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> DeleteStatusWorkFlow(string productId, string workFlowId)
        {
            return await ExecuteAsync(async () => { await ProductService.DeleteStatusWorkFlow(productId, workFlowId); return new NoContentResult(); });
        }

        /// <summary>
        /// Gets all status work flow.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <returns></returns>
        [HttpGet("{productId}/all/work-flow")]
        [ProducesResponseType(typeof(List<IWorkFlow>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetAllStatusWorkFlow(string productId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductService.GetAllStatusWorkFlow(productId));
            });
        }

        /// <summary>
        /// Gets the name of the work flow by.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="workFlowName">Name of the work flow.</param>
        /// <returns></returns>
        [HttpGet("{productId}/by/name/{workFlowName}")]
        [ProducesResponseType(typeof(IWorkFlow), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetWorkFlowByName(string productId, string workFlowName)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductService.GetWorkFlowByName(productId, workFlowName));
            });
        }

        #endregion

        #region ProductGroup

        /// <summary>
        /// Gets all groups.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <returns></returns>
        [HttpGet("{productId}/all/group")]
        [ProducesResponseType(typeof(List<IProductParameterGroup>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetAllGroups(string productId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductService.GetAllProductGroup(productId));
            });
        }

        /// <summary>
        /// Gets the product group.
        /// </summary>
        /// <param name="productGroupId">The product group identifier.</param>
        /// <returns></returns>
        [HttpGet("{productGroupId}/group")]
        [ProducesResponseType(typeof(IProductParameterGroup), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetProductGroup(string productGroupId)
        {
            return await ExecuteAsync(async () => Ok(await ProductService.GetProductGroup(productGroupId)));
        }

        /// <summary>
        /// Adds the product group.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="productGroupModel">The product group model.</param>
        /// <returns></returns>
        [HttpPost("{productId}/group")]
        [ProducesResponseType(typeof(IProductParameterGroup), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddProductGroup(string productId, [FromBody]ProductGroupRequest productGroupModel)
        {
            return await ExecuteAsync(async () => Ok(await ProductService.AddProductGroup(productId, productGroupModel)));
        }

        /// <summary>
        /// Updates the product group.
        /// </summary>
        /// <param name="productGroupId">The product group identifier.</param>
        /// <param name="productGroupModel">The product group model.</param>
        /// <returns></returns>
        [HttpPut("{productGroupId}/group")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateProductGroup(string productGroupId, [FromBody]ProductGroupRequest productGroupModel)
        {
            return await ExecuteAsync(async () => { await ProductService.UpdateProductGroup(productGroupId, productGroupModel); return new NoContentResult(); });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="productGroupId"></param>
        /// <returns></returns>
        [HttpDelete("{productGroupId}/group")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeleteProductGroup(string productGroupId)
        {
            return await ExecuteAsync(async () => { await ProductService.DeleteProductGroup(productGroupId); return new NoContentResult(); });
        }
        
        #endregion

        #region ProductParameter

        /// <summary>
        /// Adds the product parameter.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="productParametersModel">The product parameters model.</param>
        /// <returns></returns>
        [HttpPost("{productId}/parameter")]
        [ProducesResponseType(typeof(IProductParameters), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddProductParameter(string productId, [FromBody]ProductParametersRequest productParametersModel)
        {
            return await ExecuteAsync(async () => Ok(await ProductService.AddProductParameters(productId, productParametersModel)));
        }

        /// <summary>
        /// Gets all parameters.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <returns></returns>
        [HttpGet("{productId}/all/parameter")]
        [ProducesResponseType(typeof(List<IProductParameters>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetAllParameters(string productId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductService.GetAllProductParameters(productId));
            });
        }

        /// <summary>
        /// Gets the product parameter.
        /// </summary>
        /// <param name="productParameterId">The product parameter identifier.</param>
        /// <returns></returns>
        [HttpGet("{productParameterId}/parameter")]
        [ProducesResponseType(typeof(IProductParameters), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetProductParameter(string productParameterId)
        {
            return await ExecuteAsync(async () => Ok(await ProductService.GetProductParameter(productParameterId)));
        }

        /// <summary>
        /// Gets the name of the parameter by group.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="productGroupName">Name of the product group.</param>
        /// <returns></returns>
        [HttpGet("{productId}/{productGroupName}/parameter")]
        [ProducesResponseType(typeof(List<IProductParameters>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetParameterByGroupName(string productId, string productGroupName)
        {
            return await ExecuteAsync(async () => Ok(await ProductService.GetProductParameterByGroupName(productId, productGroupName)));
        }

        /// <summary>
        /// Updates the product parameter.
        /// </summary>
        /// <param name="productParameterId">The product parameter identifier.</param>
        /// <param name="productParameterModel">The product parameter model.</param>
        /// <returns></returns>
        [HttpPut("{productParameterId}/parameter")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateProductParameter(string productParameterId, [FromBody]ProductParametersRequest productParameterModel)
        {
            return await ExecuteAsync(async () => { await ProductService.UpdateProductParameters(productParameterId, productParameterModel); return new NoContentResult(); });
        }

        /// <summary>
        /// Deletes the parameter.
        /// </summary>
        /// <param name="productParameterId">The product parameter identifier.</param>
        /// <returns></returns>
        [HttpDelete("{productParameterId}/parameter")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> DeleteParameter(string productParameterId)
        {
            return await ExecuteAsync(async () => { await ProductService.DeleteProductParameters(productParameterId); return new NoContentResult(); });
        }
        
        #endregion

        #region ProductFeeParameter

        /// <summary>
        /// Adds the product fee parameter.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="productParametersModel">The product parameters model.</param>
        /// <returns></returns>
        [HttpPost("{productId}/feeparameter")]
        [ProducesResponseType(typeof(IProductFeeParameters), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddProductFeeParameter(string productId, [FromBody]ProductFeeParametersRequest productParametersModel)
        {
            return await ExecuteAsync(async () => Ok(await ProductService.AddProductFeeParameters(productId, productParametersModel)));
        }

        /// <summary>
        /// Gets all fee parameters.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <returns></returns>
        [HttpGet("{productId}/all/feeparameter")]
        [ProducesResponseType(typeof(List<IProductFeeParameters>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetAllFeeParameters(string productId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductService.GetAllProductFeeParameters(productId));
            });
        }

        /// <summary>
        /// Gets the product fee parameter.
        /// </summary>
        /// <param name="productFeeParameterId">The product fee parameter identifier.</param>
        /// <returns></returns>
        [HttpGet("{productFeeParameterId}/feeparameter")]
        [ProducesResponseType(typeof(IProductFeeParameters), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetProductFeeParameter(string productFeeParameterId)
        {
            return await ExecuteAsync(async () => Ok(await ProductService.GetProductFeeParameter(productFeeParameterId)));
        }

        /// <summary>
        /// Gets the name of the fee parameter by group.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="groupName">Name of the group.</param>
        /// <returns></returns>
        [HttpGet("{productId}/{groupName}/feeparameter")]
        [ProducesResponseType(typeof(List<IProductFeeParameters>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetFeeParameterByGroupName(string productId, string groupName)
        {
            return await ExecuteAsync(async () => Ok(await ProductService.GetProductFeeParameterByGroupName(productId, groupName)));
        }

        /// <summary>
        /// Updates the product fee parameter.
        /// </summary>
        /// <param name="productFeeParameterId">The product fee parameter identifier.</param>
        /// <param name="productFeeParameterModel">The product fee parameter model.</param>
        /// <returns></returns>
        [HttpPut("{productFeeParameterId}/feeparameter")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateProductFeeParameter(string productFeeParameterId, [FromBody]ProductFeeParametersRequest productFeeParameterModel)
        {
            return await ExecuteAsync(async () => { await ProductService.UpdateProductFeeParameters(productFeeParameterId, productFeeParameterModel); return new NoContentResult(); });
        }

        /// <summary>
        /// Deletes the fee parameter.
        /// </summary>
        /// <param name="productFeeParameterId">The product fee parameter identifier.</param>
        /// <returns></returns>
        [HttpDelete("{productFeeParameterId}/feeparameter")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> DeleteFeeParameter(string productFeeParameterId)
        {
            return await ExecuteAsync(async () => { await ProductService.DeleteProductFeeParameters(productFeeParameterId); return new NoContentResult(); });
        }

        #endregion

        /// <summary>
        /// Gets the product template.
        /// </summary>
        /// <param name="portfolioType">Type of the portfolio.</param>
        /// <returns></returns>
        [HttpGet("template/{portfolioType}")]
        [ProducesResponseType(typeof(IProductTemplate), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetProductTemplate(PortfolioType portfolioType)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductService.GetProductTemplate(portfolioType));
            });
        }
    }
}
