﻿using Moq;
using Xunit;
using LendFoundry.Foundation.Services;
using RestSharp;
using LendFoundry.ProductConfiguration.Fake;

namespace LendFoundry.ProductConfiguration.Client.Tests
{
    public class Produt_Service_Client_Test
    {
        private string ProductId = "56d6954ca5fd61a1082ed788";

        private ProdutService ProductServiceClient { get; }

        private IRestRequest Request { get; set; }

        private Mock<IServiceClient> MockServiceClient { get; }

        public Produt_Service_Client_Test()
        {
            MockServiceClient = new Mock<IServiceClient>();
            ProductServiceClient = new ProdutService(MockServiceClient.Object);
        }

        [Fact]
        public async void Product_Get_Should_Return_Not_Null_On_Success()
        {

           
            MockServiceClient.Setup(client => client.ExecuteAsync<Product>(It.IsAny<IRestRequest>())).
                Callback<IRestRequest>(restRequest => Request = restRequest).
                ReturnsAsync(new Product());

           // var produtService = new ProdutService(MockServiceClient.Object);
            var result =await ProductServiceClient.Get(ProductId);

            Assert.NotNull(result);
        }
    }
}
