﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration.Fake
{
    public class FakeProductRepository : IProductRepository
    {
        public FakeProductRepository(IProduct product)
        {
            products.Add(product);
        }
        private List<IProduct> products { get; set; } = new List<IProduct>();

        public Task<long> ActivateProduct(string productId)
        {
            throw new NotImplementedException();
        }

        public void Add(IProduct item)
        {
            throw new NotImplementedException();
        }

        public Task<long> AddCharter(string productId, List<ICharter> charter)
        {
            throw new NotImplementedException();
        }

        public Task<long> AddConsentDocument(string productId, List<IConsentDocument> consentDocument)
        {
            throw new NotImplementedException();
        }

        public Task<long> AddFeeCharge(string productId, List<IFeesCharge> feesCharge)
        {
            throw new NotImplementedException();
        }

        public Task<long> AddFundingSource(string productId, List<IFundingSource> fundingSource)
        {
            throw new NotImplementedException();
        }

        public Task<long> AddInterest(string productId, IInterest interest)
        {
            throw new NotImplementedException();
        }

        public Task<long> AddPaymentHierarchies(string productId, List<IPaymentHierarchy> paymentHierarchy)
        {
            throw new NotImplementedException();
        }

        public Task<string> AddProduct(IProduct product)
        {
            throw new NotImplementedException();
        }

        public Task<long> AddUsuryRules(string productId, List<IUsuryRules> usuryRules)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IProduct>> All(Expression<Func<IProduct, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            throw new NotImplementedException();
        }

        public int Count(Expression<Func<IProduct, bool>> query)
        {
            throw new NotImplementedException();
        }

        public Task<long> DeActivateProduct(string productId)
        {
            throw new NotImplementedException();
        }

        public Task<IProduct> Get(string id)
        {
            throw new NotImplementedException();
        }

        public Task<IProduct> GetProductDetails(string id)
        {
            throw new NotImplementedException();
        }

        public void Remove(IProduct item)
        {
            throw new NotImplementedException();
        }

        public Task<long> RemoveProduct(string productId)
        {
            throw new NotImplementedException();
        }

        public void Update(IProduct item)
        {
            throw new NotImplementedException();
        }

        public Task<long> UpdateCharter(string productId, List<ICharter> charter)
        {
            throw new NotImplementedException();
        }

        public Task<long> UpdateConsentDocument(string productId, List<IConsentDocument> consentDocument)
        {
            throw new NotImplementedException();
        }

        public Task<long> UpdateFeeCharge(string productId, List<IFeesCharge> feesCharge)
        {
            throw new NotImplementedException();
        }

        public Task<long> UpdateFundingSource(string productId, List<IFundingSource> fundingSource)
        {
            throw new NotImplementedException();
        }

        public Task<long> UpdateInterest(string productId, IInterest interest)
        {
            throw new NotImplementedException();
        }

        public Task<long> UpdatePaymentHierarchies(string productId, List<IPaymentHierarchy> paymentHierarchy)
        {
            throw new NotImplementedException();
        }

        public Task<long> UpdateProduct(IProduct product)
        {
            throw new NotImplementedException();
        }

        public Task<long> UpdateProduct(string productId, IProduct product)
        {
            throw new NotImplementedException();
        }

        public Task<long> UpdateUsuryRules(string productId, List<IUsuryRules> usuryRules)
        {
            throw new NotImplementedException();
        }
    }
}
