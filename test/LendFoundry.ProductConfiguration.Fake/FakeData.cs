﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductConfiguration.Fake
{
    public class FakeData
    {
        public static IProductRequestModel FakeValidProductRequestModel => new ProductRequestModel()
        {
            Name = "Home Loan",
            Description = "Loan for Home",
            FamilyId = "1",
            Accruals = new Interest()
            {
                InterestType = InterestType.Fixed,
                CompoundingPeriod = FrequencyType.Annual,
                TermDuration = TermDuration.Month,
                SupportedLoanTerms = new List<LoanTerm>()
                        {
                           new LoanTerm()
                           {
                               Frequency=  FrequencyType.Annual,
                               Term =12
                           },
                           new LoanTerm()
                           {
                               Frequency=  FrequencyType.Annual,
                               Term =36
                           }
                        },
                PaymentFrequency = FrequencyType.Monthly,
                PaymentType = PaymentType.Installment,
                PaymentTime = PaymentTime.BeginningOfPeriod,
                AmortizationCalendar = AmortizationCalendar.Year360Days,
                FirstPaymentDate = "10/10/2016",
                SupportDeferredInterest = true,
                DaysToStartInterestAccrual = "1"
            },
            Charters = new List<Charter>()
            {
                new Charter()
                {
                    BankId = 1,
                    SupportedGeographies = new Geographies()
                    {
                        Country= new Country()
                        {
                            Name= "USA",
                            Code= "01"
                        },
                        ExcludedStates= new List<State>()
                        {
                            new State()
                            {
                                CountryCode="01",
                                Name="West Verginia",
                                Code="WV"
                            }
                        }
                    },
                }
            },
            FundingSources = new List<FundingSource>()
            {
                new FundingSource()
                {
                    Name="Test Funding Source"
                }
            },
            PaymentHierarchies = new List<PaymentHierarchy>()
            {
                new PaymentHierarchy()
                {
                    Priority=1,
                    PaymentElement= new PaymentElement()
                    {
                        Name="Test Element",
                        Description="Test Element Desc"
                    }
                }
            },
            FeeCharges = new List<FeesCharge>()
            {
                new FeesCharge()
                {
                    FeeType =  FeeType.LateFee,
                    FeeDescription = "Test Description",
                    FeeAmount = new Rule()
                    {
                        Name ="Test Rule",
                        Description ="Test Description"
                    },
                    Applicability = new Rule()
                    {
                        Name ="Test Rule",
                        Description ="Test Description"
                    },
                    ApplyInterest = true,
                    Amortize = true,
                    StartDate = default(DateTimeOffset),
                    ExpiresOn = default(DateTimeOffset),
                    ChartOfAccountMapping = 1
                }
            },
            Documents = new List<ConsentDocument>()
            {
                new ConsentDocument()
                {
                    ConsentPurpose = "Test",
                    DocumentDisplayName = "Test Document Name",
                    ValidFrom = default(DateTimeOffset),
                    ExpiresOn = default(DateTimeOffset),
                    DocumentUrl= ""
                }
            },
            UsaryRules = new List<UsuryRules>()
            {
                new UsuryRules()
                {
                    RulePurpose = "ID Verification",
                    Rule = "Test Rile",
                    StartDate = default(DateTimeOffset),
                    ExpiresOn = default(DateTimeOffset),
                }
            }
        };

        public static IProductRequestModel FakeInValidProductRequestModel => new ProductRequestModel()
        {
            Name = "",
            Description = "Loan for Home",
            FamilyId = "",
            Accruals = new Interest()
            {
                InterestType = InterestType.Fixed,
                CompoundingPeriod = FrequencyType.Annual,
                TermDuration = TermDuration.Month,
                SupportedLoanTerms = new List<LoanTerm>()
                        {
                           new LoanTerm()
                           {
                               Frequency= FrequencyType.Annual,
                               Term =12
                           },
                           new LoanTerm()
                           {
                               Frequency= FrequencyType.Annual,
                               Term =36
                           }
                        },
                PaymentFrequency = FrequencyType.Annual,
                PaymentType = PaymentType.OneTime,
                PaymentTime = PaymentTime.EndOfPeriod,
                AmortizationCalendar = AmortizationCalendar.Year360Days,
                FirstPaymentDate = "10/10/2016",
                SupportDeferredInterest = true,
                DaysToStartInterestAccrual = "1"
            },
            Charters = new List<Charter>()
            {
                new Charter()
                {
                    BankId = 1,
                    SupportedGeographies = new Geographies()
                    {
                        Country= new Country()
                        {
                            Name= "USA",
                            Code= "01"
                        },
                        ExcludedStates= new List<State>()
                        {
                            new State()
                            {
                                CountryCode="01",
                                Name="West Verginia",
                                Code="WV"
                            }
                        }
                    },
                }
            },
            FundingSources = new List<FundingSource>()
            {
                new FundingSource()
                {
                    Name="Test Funding Source"
                }
            },
            PaymentHierarchies = new List<PaymentHierarchy>()
            {
                new PaymentHierarchy()
                {
                    Priority=1,
                    PaymentElement= new PaymentElement()
                    {
                        Name="Test Element",
                        Description="Test Element Desc"
                    }
                }
            },
            FeeCharges = new List<FeesCharge>()
            {
                new FeesCharge()
                {
                    FeeType = FeeType.LateFee,
                    FeeDescription = "Test Description",
                    FeeAmount = new Rule()
                    {
                        Name ="Test Rule",
                        Description ="Test Description"
                    },
                    Applicability = new Rule()
                    {
                        Name ="Test Rule",
                        Description ="Test Description"
                    },
                    ApplyInterest = true,
                    Amortize = true,
                    StartDate = default(DateTimeOffset),
                    ExpiresOn = default(DateTimeOffset),
                    ChartOfAccountMapping = 1
                }
            },
            Documents = new List<ConsentDocument>()
            {
                new ConsentDocument()
                {
                    ConsentPurpose = "Test",
                    DocumentDisplayName = "Test Document Name",
                    ValidFrom = default(DateTimeOffset),
                    ExpiresOn = default(DateTimeOffset),
                    DocumentUrl= ""
                }
            },
            UsaryRules = new List<UsuryRules>()
            {
                new UsuryRules()
                {
                    RulePurpose = "ID Verification",
                    Rule = "Test Rile",
                    StartDate = default(DateTimeOffset),
                    ExpiresOn = default(DateTimeOffset),
                }
            }
        };

        public static IProduct FakeValidProductData => new Product()
        {
            Id = "56d6954ca5fd61a1082ed788",
            FamilyId = "1",
            TenantId = "my-tenant",
            Name = "Home Loan",
            Description = "Home Loan Description",
            Status = ProductStatus.Add,
            StartDate = default(DateTimeOffset),
            ExpiresOn = default(DateTimeOffset),
            ProductCharacteristics = new ProductCharacteristics()
            {
                Accruals = new Interest()
                {
                    InterestType = InterestType.Fixed,
                    PaymentFrequency = FrequencyType.Annual,
                    PaymentType = PaymentType.Installment,
                    PaymentTime = PaymentTime.BeginningOfPeriod,
                    AmortizationCalendar = AmortizationCalendar.Year360Days,
                    CompoundingPeriod = FrequencyType.Annual,
                    DaysToStartInterestAccrual = "",
                    FirstPaymentDate = "",
                    SupportDeferredInterest = true,
                    SupportedLoanTerms = new List<LoanTerm>()
                    {
                        new LoanTerm()
                        {
                            Term= 12,
                            Frequency=  FrequencyType.Annual
                        }
                    },
                    TermDuration = TermDuration.Month
                },
                Charters = new List<Charter>()
                {
                    new Charter()
                    {
                        BankId=1,
                        SupportedGeographies= new Geographies()
                        {
                            Country= new Country()
                            {
                                Code="",
                                Name=""
                            },
                            ExcludedStates=new List<State>()
                            {
                                new State()
                                {
                                    Code="",
                                    Name="",
                                    CountryCode=""
                                }
                            },
                            IncludedStates= new List<State>()
                            {
                                new State()
                                {
                                    Code="",
                                    Name="",
                                    CountryCode=""
                                }
                            }
                        }
                    }
                },
                FundingSources = new List<FundingSource>()
                {
                    new FundingSource()
                    {
                        Name=""
                    }
                },
                PaymentHierarchies = new List<PaymentHierarchy>()
                {
                    new PaymentHierarchy()
                    {
                        Priority= 1,
                        PaymentElement= new PaymentElement()
                        {
                            Name="",
                            Description=""
                        }
                    }
                },
                FeeCharges = new List<FeesCharge>()
                {
                    new FeesCharge()
                    {
                        Amortize= true,
                        Applicability= new Rule()
                        {
                            Name="",
                            Description=""
                        },
                        ApplyInterest=false,
                        ChartOfAccountMapping= 1,
                        ExpiresOn= default(DateTimeOffset),
                        FeeAmount=new Rule()
                        {
                            Name="",
                            Description=""
                        },
                        FeeDescription="",
                        FeeType=  FeeType.LateFee,
                        PaymentElement= new PaymentElement()
                        {
                            Name="",
                            Description=""
                        },
                        StartDate=default(DateTimeOffset)
                    }
                },
                Documents = new List<ConsentDocument>()
                {
                    new ConsentDocument()
                    {
                        ConsentPurpose="",
                        DocumentUrl= "",
                        DocumentDisplayName="",
                        ExpiresOn=default(DateTimeOffset),
                        ValidFrom= default(DateTimeOffset)
                    }
                },
                UsaryRules = new List<UsuryRules>()
                {
                     new UsuryRules()
                     {
                         ApplicableGeography=new Geographies()
                         {
                             Country= new Country()
                            {
                                Code="",
                                Name=""
                            },
                            ExcludedStates=new List<State>()
                            {
                                new State()
                                {
                                    Code="",
                                    Name="",
                                    CountryCode=""
                                }
                            },
                            IncludedStates= new List<State>()
                            {
                                new State()
                                {
                                    Code="",
                                    Name="",
                                    CountryCode=""
                                }
                            }
                         },
                       Rule="",
                       RulePurpose="",
                       ExpiresOn=default(DateTimeOffset),
                       StartDate=default(DateTimeOffset)
                     }
                }
            }
        };

    }
}
