﻿using Xunit;
using Moq;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Date;
using LendFoundry.EventHub.Client;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Mvc;
using LendFoundry.ProductConfiguration.Api.Controllers;
using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Services;

namespace LendFoundry.ProductConfiguration.Api.Tests
{
    public class Product_Controller
    {
        private Mock<ILogger> logger { get; } = new Mock<ILogger>();
        private Mock<ITenantService> tenantService
        {
            get
            {
                var tenant = new Mock<ITenantService>();
                tenant.SetupAllProperties();
                tenant.SetupGet(x => x.Current).Returns(new TenantInfo("Tenant"));
                return tenant;
                // new Mock<ITenantService>().SetupAllProperties().SetupGet(x => x.Current).Returns(new TenantInfo("Tenant"));
            }
        }
        private Mock<ITenantTime> tenantTime { get; } = new Mock<ITenantTime>();

        private Mock<IEventHubClient> eventHub { get; } = new Mock<IEventHubClient>();
        private Mock<IProductConfiguration> productConfig { get; } = new Mock<IProductConfiguration>();
        private Mock<IProductService> productService { get; } = new Mock<IProductService>();

        private ProductController productController
        {
            get
            {
                return new ProductController(logger.Object,
                eventHub.Object,
                tenantService.Object,
                tenantTime.Object,
                productConfig.Object,
                productService.Object);
            }
        }

        private string ProductId = "56d6954ca5fd61a1082ed788";

        private ProductRequestModel ModelToAdd
        {
            get
            {
                return new ProductRequestModel()
                {
                    Name = "Home Loan",
                    Description = "Loan for Home",
                    FamilyId = "1",
                    Accruals = new Interest()
                    {
                        InterestType =  InterestType.Fixed,
                        CompoundingPeriod = FrequencyType.Annual,
                        TermDuration = TermDuration.Month,
                        SupportedLoanTerms = new List<LoanTerm>()
                        {
                           new LoanTerm()
                           {
                               Frequency= FrequencyType.Annual,
                               Term =12
                           },
                           new LoanTerm()
                           {
                               Frequency= FrequencyType.Annual,
                               Term =36
                           }
                        },
                        PaymentFrequency = FrequencyType.Annual,
                        PaymentType =  PaymentType.OneTime,
                        PaymentTime =  PaymentTime.BeginningOfPeriod,
                        AmortizationCalendar =  AmortizationCalendar.Year360Days,
                        FirstPaymentDate = "10/10/2016",
                        SupportDeferredInterest = true,
                        DaysToStartInterestAccrual = "1"
                    },
                    Charters = new List<Charter>()
                    {
                        new Charter()
                        {
                            BankId = 1,
                            SupportedGeographies = new Geographies()
                            {
                                Country= new Country()
                                {
                                    Name= "USA",
                                    Code= "01"
                                },
                                ExcludedStates= new List<State>()
                                {
                                    new State()
                                    {
                                        CountryCode="01",
                                        Name="West Verginia",
                                        Code="WV"
                                    }
                                }
                            },
                        }
                    },
                    FundingSources = new List<FundingSource>()
                    {
                        new FundingSource()
                        {
                            Name="Test Funding Source"
                        }
                    },
                    PaymentHierarchies = new List<PaymentHierarchy>()
                    {
                        new PaymentHierarchy()
                        {
                            Priority=1,
                            PaymentElement= new PaymentElement()
                            {
                                Name="Test Element",
                                Description="Test Element Desc"
                            }
                        }
                    },
                    FeeCharges = new List<FeesCharge>()
                    {
                        new FeesCharge()
                        {
                            FeeType = FeeType.LateFee,
                            FeeDescription = "Test Description",
                            FeeAmount = new Rule()
                            {
                                Name ="Test Rule",
                                Description ="Test Description"
                            },
                            Applicability = new Rule()
                            {
                                Name ="Test Rule",
                                Description ="Test Description"
                            },
                            ApplyInterest = true,
                            Amortize = true,
                            StartDate = tenantTime.Object.Now,
                            ExpiresOn = tenantTime.Object.Now,
                            ChartOfAccountMapping = 1
                        }
                    },
                    Documents = new List<ConsentDocument>()
                    {
                        new ConsentDocument()
                        {
                            ConsentPurpose = "Test",
                            DocumentDisplayName = "Test Document Name",
                            ValidFrom = tenantTime.Object.Now,
                            ExpiresOn = tenantTime.Object.Now,
                        }
                    },
                    UsaryRules = new List<UsuryRules>()
                    {
                        new UsuryRules()
                        {
                            RulePurpose = "ID Verification",
                            Rule = "Test Rile",
                            StartDate = tenantTime.Object.Now,
                            ExpiresOn = tenantTime.Object.Now,
                        }
                    }
                };
            }
        }

        #region Get Product

        [Fact]
        public async  void Product_Get_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            productService.
                Setup(x => x.Get(It.IsAny<string>())).
               ReturnsAsync(new Product());

            var result =await productController.Get(ProductId) as ObjectResult;

            productService.Verify(x => x.Get(It.IsAny<string>()), Times.Once());

            Assert.NotNull(result.Value);
            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void Product_Get_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
                Setup(x => x.Get(It.IsAny<string>())).
                Throws(new ArgumentException("Product Id should not empty."));

            var result =await productController.Get(string.Empty) as ObjectResult;

            productService.Verify(x => x.Get(It.IsAny<string>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void Product_Get_Should_Return_404_On_Not_Found()
        {
            var expectedValue = 404;

            productService.
                Setup(x => x.Get(It.IsAny<string>())).
                Throws(new NotFoundException("Product not found"));

            var result =await productController.Get(string.Empty) as ObjectResult;

            productService.Verify(x => x.Get(It.IsAny<string>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Add Product

        [Fact]
        public async void Product_Add_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            productService.
                Setup(x => x.Add(It.IsAny<IProductRequestModel>())).
                ReturnsAsync(ProductId);

            var result =await productController.AddProduct(ModelToAdd) as ObjectResult;

            productService.Verify(x => x.Add(It.IsAny<IProductRequestModel>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void Product_Add_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
                Setup(x => x.Add(It.IsAny<IProductRequestModel>())).
                Throws(new ArgumentNullException(nameof(ModelToAdd)));

            var result =await productController.AddProduct(null) as ObjectResult;

            productService.Verify(x => x.Add(It.IsAny<IProductRequestModel>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Update Product

        [Fact]
        public async void Product_Update_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //    Setup(x => x.Update(It.IsAny<string>(),It.IsAny<IProductRequestModel>()));

            var result =await productController.UpdateProduct(ProductId, ModelToAdd) as HttpOkResult;

            productService.Verify(x => x.Update(It.IsAny<string>(),It.IsAny<IProductRequestModel>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void Product_Update_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
                Setup(x => x.Update(It.IsAny<string>(),It.IsAny<IProductRequestModel>())).
                Throws(new ArgumentNullException(nameof(ModelToAdd)));

            var result =await productController.UpdateProduct(ProductId, ModelToAdd) as ObjectResult;

            productService.Verify(x => x.Update(It.IsAny<string>(),It.IsAny<IProductRequestModel>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void Product_Update_Should_Return_404_On_Not_Found()
        {
            var expectedValue = 404;

            productService.
               Setup(x => x.Update(It.IsAny<string>(),It.IsAny<IProductRequestModel>())).
               Throws(new NotFoundException("Product not found"));

            var result =await productController.UpdateProduct(ProductId, ModelToAdd) as ObjectResult;

            productService.Verify(x => x.Update(It.IsAny<string>(),It.IsAny<IProductRequestModel>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Delete Product

        [Fact]
        public async void Product_Delete_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //    Setup(x => x.Delete(It.IsAny<string>()));

            var result =await productController.DeleteProduct(ProductId) as HttpOkResult;

            productService.Verify(x => x.Delete(It.IsAny<string>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void Product_Delete_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
                Setup(x => x.Delete(It.IsAny<string>())).
                Throws(new ArgumentNullException(nameof(ModelToAdd)));

            var result =await productController.DeleteProduct(ProductId) as ObjectResult;

            productService.Verify(x => x.Delete(It.IsAny<string>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void Product_Delete_Should_Return_404_On_Not_Found()
        {
            var expectedValue = 404;

            productService.
               Setup(x => x.Delete(It.IsAny<string>())).
               Throws(new NotFoundException("Product not found"));

            var result =await productController.DeleteProduct(ProductId) as ObjectResult;

            productService.Verify(x => x.Delete(It.IsAny<string>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Activate Product

        [Fact]
        public async void Product_Activate_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //    Setup(x => x.Activate(It.IsAny<string>()));

            var result =await productController.ActivateProduct(ProductId) as HttpOkResult;

            productService.Verify(x => x.Activate(It.IsAny<string>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void Product_Activate_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
                Setup(x => x.Activate(It.IsAny<string>())).
                Throws(new ArgumentNullException(nameof(ModelToAdd)));

            var result =await productController.ActivateProduct(ProductId) as ObjectResult;

            productService.Verify(x => x.Activate(It.IsAny<string>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void Product_Activate_Should_Return_404_On_Not_Found()
        {
            var expectedValue = 404;

            productService.
               Setup(x => x.Activate(It.IsAny<string>())).
               Throws(new NotFoundException("Product not found"));

            var result =await productController.ActivateProduct(ProductId) as ObjectResult;

            productService.Verify(x => x.Activate(It.IsAny<string>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region DeActivate Product

        [Fact]
        public async void Product_DeActivate_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //    Setup(x => x.Expire(It.IsAny<string>()));

            var result =await productController.ExpireProduct(ProductId) as HttpOkResult;

            productService.Verify(x => x.Expire(It.IsAny<string>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void Product_DeActivate_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
                Setup(x => x.Expire(It.IsAny<string>())).
                Throws(new ArgumentNullException(nameof(ModelToAdd)));

            var result =await productController.ExpireProduct(ProductId) as ObjectResult;

            productService.Verify(x => x.Expire(It.IsAny<string>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void Product_DeActivate_Should_Return_404_On_Not_Found()
        {
            var expectedValue = 404;

            productService.
               Setup(x => x.Expire(It.IsAny<string>())).
               Throws(new NotFoundException("Product not found"));

            var result =await productController.ExpireProduct(ProductId) as ObjectResult;

            productService.Verify(x => x.Expire(It.IsAny<string>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Add Interest

        [Fact]
        public async void Interest_Add_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //    Setup(x => x.AddInterest(It.IsAny<string>(), It.IsAny<IInterest>()));

            var result =await productController.AddInterest(ProductId, ModelToAdd.Accruals) as HttpOkResult;

            productService.Verify(x => x.AddInterest(It.IsAny<string>(), It.IsAny<IInterest>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void Interest_Add_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
                Setup(x => x.AddInterest(It.IsAny<string>(), It.IsAny<IInterest>())).
            Throws(new ArgumentNullException(nameof(ModelToAdd.Accruals)));

            var result =await productController.AddInterest(ProductId, ModelToAdd.Accruals) as ObjectResult;

            productService.Verify(x => x.AddInterest(It.IsAny<string>(), It.IsAny<IInterest>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Update Interest

        [Fact]
        public async void Interest_Update_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //         Setup(x => x.UpdateInterest(It.IsAny<string>(), It.IsAny<IInterest>())).Verifiable();

           var result = await productController.UpdateInterest(ProductId, ModelToAdd.Accruals) as HttpOkResult;
        
            productService.Verify(x => x.UpdateInterest(It.IsAny<string>(), It.IsAny<IInterest>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void Interest_Update_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
                Setup(x => x.UpdateInterest(It.IsAny<string>(), It.IsAny<IInterest>())).
                Throws(new ArgumentNullException(nameof(ModelToAdd)));

            var result =await productController.UpdateInterest(ProductId, ModelToAdd.Accruals) as ObjectResult;

            productService.Verify(x => x.UpdateInterest(It.IsAny<string>(), It.IsAny<IInterest>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void Interest_Update_Should_Return_404_On_Not_Found()
        {
            var expectedValue = 404;

            productService.
                Setup(x => x.UpdateInterest(It.IsAny<string>(), It.IsAny<IInterest>())).
                Throws(new NotFoundException("Product not found"));

            var result =await productController.UpdateInterest(ProductId, ModelToAdd.Accruals) as ObjectResult;

            productService.Verify(x => x.UpdateInterest(It.IsAny<string>(), It.IsAny<IInterest>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Add Charter

        [Fact]
        public async void Charter_Add_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //    Setup(x => x.AddCharter(It.IsAny<string>(), It.IsAny<List<ICharter>>()));

            var result =await productController.AddCharter(ProductId, ModelToAdd.Charters) as HttpOkResult;

            productService.Verify(x => x.AddCharter(It.IsAny<string>(), It.IsAny<List<ICharter>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void Charter_Add_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
               Setup(x => x.AddCharter(It.IsAny<string>(), It.IsAny<List<ICharter>>())).
               Throws(new ArgumentNullException(nameof(ModelToAdd.Charters)));

            var result =await productController.AddCharter(ProductId, ModelToAdd.Charters) as ObjectResult;

            productService.Verify(x => x.AddCharter(It.IsAny<string>(), It.IsAny<List<ICharter>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Update Charter

        [Fact]
        public async void Charter_Update_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //    Setup(x => x.UpdateCharter(It.IsAny<string>(), It.IsAny<List<ICharter>>()));

            var result =await productController.UpdateCharter(ProductId, ModelToAdd.Charters) as HttpOkResult;

            productService.Verify(x => x.UpdateCharter(It.IsAny<string>(), It.IsAny<List<ICharter>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void Charter_Update_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
                Setup(x => x.UpdateCharter(It.IsAny<string>(), It.IsAny<List<ICharter>>())).
                Throws(new ArgumentNullException(nameof(ModelToAdd.Charters)));

            var result =await productController.UpdateCharter(ProductId, ModelToAdd.Charters) as ObjectResult;

            productService.Verify(x => x.UpdateCharter(It.IsAny<string>(), It.IsAny<List<ICharter>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void Charter_Update_Should_Return_404_On_Not_Found()
        {
            var expectedValue = 404;

            productService.
                 Setup(x => x.UpdateCharter(It.IsAny<string>(), It.IsAny<List<ICharter>>())).
                Throws(new NotFoundException("Product not found"));

            var result =await productController.UpdateCharter(ProductId, ModelToAdd.Charters) as ObjectResult;

            productService.Verify(x => x.UpdateCharter(It.IsAny<string>(), It.IsAny<List<ICharter>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Add Funding Source

        [Fact]
        public async void FundingSource_Add_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //    Setup(x => x.AddFundingSource(It.IsAny<string>(), It.IsAny<List<IFundingSource>>()));

            var result =await productController.AddFundingSource(ProductId, ModelToAdd.FundingSources) as HttpOkResult;

            productService.Verify(x => x.AddFundingSource(It.IsAny<string>(), It.IsAny<List<IFundingSource>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void FundingSource_Add_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
                 Setup(x => x.AddFundingSource(It.IsAny<string>(), It.IsAny<List<IFundingSource>>())).
               Throws(new ArgumentNullException(nameof(ModelToAdd.FundingSources)));

            var result =await productController.AddFundingSource(ProductId, ModelToAdd.FundingSources) as ObjectResult;

            productService.Verify(x => x.AddFundingSource(It.IsAny<string>(), It.IsAny<List<IFundingSource>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Update Funding Source

        [Fact]
        public async void FundingSource_Update_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //    Setup(x => x.UpdateFundingSource(It.IsAny<string>(), It.IsAny<List<IFundingSource>>()));

            var result =await productController.UpdateFundingSource(ProductId, ModelToAdd.FundingSources) as HttpOkResult;

            productService.Verify(x => x.UpdateFundingSource(It.IsAny<string>(), It.IsAny<List<IFundingSource>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void FundingSource_Update_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
                 Setup(x => x.UpdateFundingSource(It.IsAny<string>(), It.IsAny<List<IFundingSource>>())).
                Throws(new ArgumentNullException(nameof(ModelToAdd.FundingSources)));

            var result =await productController.UpdateFundingSource(ProductId, ModelToAdd.FundingSources) as ObjectResult;

            productService.Verify(x => x.UpdateFundingSource(It.IsAny<string>(), It.IsAny<List<IFundingSource>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void FundingSource_Update_Should_Return_404_On_Not_Found()
        {
            var expectedValue = 404;

            productService.
                 Setup(x => x.UpdateFundingSource(It.IsAny<string>(), It.IsAny<List<IFundingSource>>())).
                Throws(new NotFoundException("Product not found"));

            var result =await productController.UpdateFundingSource(ProductId, ModelToAdd.FundingSources) as ObjectResult;

            productService.Verify(x => x.UpdateFundingSource(It.IsAny<string>(), It.IsAny<List<IFundingSource>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Add Payment Hierarchies

        [Fact]
        public async void PaymentHierarchies_Add_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //    Setup(x => x.AddPaymentHierarchies(It.IsAny<string>(), It.IsAny<List<IPaymentHierarchy>>()));

            var result =await productController.AddPaymentHierarchies(ProductId, ModelToAdd.PaymentHierarchies) as HttpOkResult;

            productService.Verify(x => x.AddPaymentHierarchies(It.IsAny<string>(), It.IsAny<List<IPaymentHierarchy>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void PaymentHierarchies_Add_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
                Setup(x => x.AddPaymentHierarchies(It.IsAny<string>(), It.IsAny<List<IPaymentHierarchy>>())).
               Throws(new ArgumentNullException(nameof(ModelToAdd.PaymentHierarchies)));

            var result =await productController.AddPaymentHierarchies(ProductId, ModelToAdd.PaymentHierarchies) as ObjectResult;

            productService.Verify(x => x.AddPaymentHierarchies(It.IsAny<string>(), It.IsAny<List<IPaymentHierarchy>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Update Payment Hierarchies

        [Fact]
        public async void PaymentHierarchies_Update_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //  Setup(x => x.UpdatePaymentHierarchies(It.IsAny<string>(), It.IsAny<List<IPaymentHierarchy>>()));

            var result =await productController.UpdatePaymentHierarchies(ProductId, ModelToAdd.PaymentHierarchies) as HttpOkResult;

            productService.Verify(x => x.UpdatePaymentHierarchies(It.IsAny<string>(), It.IsAny<List<IPaymentHierarchy>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void PaymentHierarchies_Update_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
              Setup(x => x.UpdatePaymentHierarchies(It.IsAny<string>(), It.IsAny<List<IPaymentHierarchy>>())).
                Throws(new ArgumentNullException(nameof(ModelToAdd.PaymentHierarchies)));

            var result =await productController.UpdatePaymentHierarchies(ProductId, ModelToAdd.PaymentHierarchies) as ObjectResult;

            productService.Verify(x => x.UpdatePaymentHierarchies(It.IsAny<string>(), It.IsAny<List<IPaymentHierarchy>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void PaymentHierarchies_Update_Should_Return_404_On_Not_Found()
        {
            var expectedValue = 404;

            productService.
              Setup(x => x.UpdatePaymentHierarchies(It.IsAny<string>(), It.IsAny<List<IPaymentHierarchy>>())).
                Throws(new NotFoundException("Product not found"));

            var result =await productController.UpdatePaymentHierarchies(ProductId, ModelToAdd.PaymentHierarchies) as ObjectResult;

            productService.Verify(x => x.UpdatePaymentHierarchies(It.IsAny<string>(), It.IsAny<List<IPaymentHierarchy>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Add Fees Charge

        [Fact]
        public async void FeesCharge_Add_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //    Setup(x => x.AddFeesCharge(It.IsAny<string>(), It.IsAny<List<IFeesCharge>>()));

            var result =await productController.AddFeesCharge(ProductId, ModelToAdd.FeeCharges) as HttpOkResult;

            productService.Verify(x => x.AddFeesCharge(It.IsAny<string>(), It.IsAny<List<IFeesCharge>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void FeesCharge_Add_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
                 Setup(x => x.AddFeesCharge(It.IsAny<string>(), It.IsAny<List<IFeesCharge>>())).
                 Throws(new ArgumentNullException(nameof(ModelToAdd.FeeCharges)));

            var result =await productController.AddFeesCharge(ProductId, ModelToAdd.FeeCharges) as ObjectResult;

            productService.Verify(x => x.AddFeesCharge(It.IsAny<string>(), It.IsAny<List<IFeesCharge>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Update Fees Charge

        [Fact]
        public async void FeesCharge_Update_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //    Setup(x => x.UpdateFeesCharge(It.IsAny<string>(), It.IsAny<List<IFeesCharge>>()));

            var result =await productController.UpdateFeesCharge(ProductId, ModelToAdd.FeeCharges) as HttpOkResult;

            productService.Verify(x => x.UpdateFeesCharge(It.IsAny<string>(), It.IsAny<List<IFeesCharge>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void FeesCharge_Update_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
                Setup(x => x.UpdateFeesCharge(It.IsAny<string>(), It.IsAny<List<IFeesCharge>>())).
                Throws(new ArgumentNullException(nameof(ModelToAdd.FeeCharges)));

            var result =await productController.UpdateFeesCharge(ProductId, ModelToAdd.FeeCharges) as ObjectResult;

            productService.Verify(x => x.UpdateFeesCharge(It.IsAny<string>(), It.IsAny<List<IFeesCharge>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void FeesCharge_Update_Should_Return_404_On_Not_Found()
        {
            var expectedValue = 404;

            productService.
               Setup(x => x.UpdateFeesCharge(It.IsAny<string>(), It.IsAny<List<IFeesCharge>>())).
               Throws(new NotFoundException("Product not found"));

            var result =await productController.UpdateFeesCharge(ProductId, ModelToAdd.FeeCharges) as ObjectResult;

            productService.Verify(x => x.UpdateFeesCharge(It.IsAny<string>(), It.IsAny<List<IFeesCharge>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Add Consent Document

        [Fact]
        public async void ConsentDocument_Add_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //    Setup(x => x.AddConsentDocument(It.IsAny<string>(), It.IsAny<List<IConsentDocument>>()));

            var result =await productController.AddConsentDocument(ProductId, ModelToAdd.Documents) as HttpOkResult;

            productService.Verify(x => x.AddConsentDocument(It.IsAny<string>(), It.IsAny<List<IConsentDocument>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void ConsentDocument_Add_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
                Setup(x => x.AddConsentDocument(It.IsAny<string>(), It.IsAny<List<IConsentDocument>>())).
                 Throws(new ArgumentNullException(nameof(ModelToAdd.Documents)));

            var result =await productController.AddConsentDocument(ProductId, ModelToAdd.Documents) as ObjectResult;

            productService.Verify(x => x.AddConsentDocument(It.IsAny<string>(), It.IsAny<List<IConsentDocument>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Update Consent Document

        [Fact]
        public async void ConsentDocument_Update_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //    Setup(x => x.UpdateConsentDocument(It.IsAny<string>(), It.IsAny<List<IConsentDocument>>()));

            var result =await productController.UpdateConsentDocument(ProductId, ModelToAdd.Documents) as HttpOkResult;

            productService.Verify(x => x.UpdateConsentDocument(It.IsAny<string>(), It.IsAny<List<IConsentDocument>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void ConsentDocument_Update_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
                 Setup(x => x.UpdateConsentDocument(It.IsAny<string>(), It.IsAny<List<IConsentDocument>>())).
                Throws(new ArgumentNullException(nameof(ModelToAdd.Documents)));

            var result =await productController.UpdateConsentDocument(ProductId, ModelToAdd.Documents) as ObjectResult;

            productService.Verify(x => x.UpdateConsentDocument(It.IsAny<string>(), It.IsAny<List<IConsentDocument>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void ConsentDocument_Update_Should_Return_404_On_Not_Found()
        {
            var expectedValue = 404;

            productService.
                  Setup(x => x.UpdateConsentDocument(It.IsAny<string>(), It.IsAny<List<IConsentDocument>>())).
                Throws(new NotFoundException("Product not found"));

            var result =await productController.UpdateConsentDocument(ProductId, ModelToAdd.Documents) as ObjectResult;

            productService.Verify(x => x.UpdateConsentDocument(It.IsAny<string>(), It.IsAny<List<IConsentDocument>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Add Usury Rules

        [Fact]
        public async void UsuryRules_Add_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //    Setup(x => x.AddUsuryRule(It.IsAny<string>(), It.IsAny<List<IUsuryRules>>()));

            var result =await productController.AddUsuryRule(ProductId, ModelToAdd.UsaryRules) as HttpOkResult;

            productService.Verify(x => x.AddUsuryRule(It.IsAny<string>(), It.IsAny<List<IUsuryRules>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void UsuryRules_Add_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
                Setup(x => x.AddUsuryRule(It.IsAny<string>(), It.IsAny<List<IUsuryRules>>())).
                 Throws(new ArgumentNullException(nameof(ModelToAdd.Documents)));

            var result =await productController.AddUsuryRule(ProductId, ModelToAdd.UsaryRules) as ObjectResult;

            productService.Verify(x => x.AddUsuryRule(It.IsAny<string>(), It.IsAny<List<IUsuryRules>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion

        #region Update Usury Rules

        [Fact]
        public async void UsuryRules_Update_Should_Return_200_On_Success()
        {
            var expectedValue = 200;

            //productService.
            //   Setup(x => x.UpdateUsuryRule(It.IsAny<string>(), It.IsAny<List<IUsuryRules>>()));

            var result =await productController.UpdateUsuryRule(ProductId, ModelToAdd.UsaryRules) as HttpOkResult;

            productService.Verify(x => x.UpdateUsuryRule(It.IsAny<string>(), It.IsAny<List<IUsuryRules>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void UsuryRules_Update_Should_Return_400_On_Validation_Fail()
        {
            var expectedValue = 400;

            productService.
               Setup(x => x.UpdateUsuryRule(It.IsAny<string>(), It.IsAny<List<IUsuryRules>>())).
                Throws(new ArgumentNullException(nameof(ModelToAdd.UsaryRules)));

            var result =await productController.UpdateUsuryRule(ProductId, ModelToAdd.UsaryRules) as ObjectResult;

            productService.Verify(x => x.UpdateUsuryRule(It.IsAny<string>(), It.IsAny<List<IUsuryRules>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        [Fact]
        public async void UsuryRules_Update_Should_Return_404_On_Not_Found()
        {
            var expectedValue = 404;

            productService.
                Setup(x => x.UpdateUsuryRule(It.IsAny<string>(), It.IsAny<List<IUsuryRules>>())).
                 Throws(new NotFoundException("Product not found"));

            var result =await productController.UpdateUsuryRule(ProductId, ModelToAdd.UsaryRules) as ObjectResult;

            productService.Verify(x => x.UpdateUsuryRule(It.IsAny<string>(), It.IsAny<List<IUsuryRules>>()), Times.Once());

            Assert.Equal(expectedValue, result.StatusCode);
        }

        #endregion
    }
}
