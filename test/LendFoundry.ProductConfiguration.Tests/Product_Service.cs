﻿using Xunit;
using Moq;
using LendFoundry.EventHub.Client;
using Microsoft.AspNet.Mvc;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using System;
using LendFoundry.Foundation.Services;
using System.Linq;
using LendFoundry.DocumentManager;

namespace LendFoundry.ProductConfiguration.Tests
{
    public class Product_Service
    {
        private Mock<IEventHubClient> eventHub { get; } = new Mock<IEventHubClient>();
        private Mock<IProductRepository> productRepository { get; } = new Mock<IProductRepository>();

        private Mock<IDocumentManagerService> DocumentManager { get; } = new Mock<IDocumentManagerService>();
        private Mock<ITenantTime> tenantTime { get; } = new Mock<ITenantTime>();
        private ProductService productService
        {
            get
            {
                return new ProductService(productRepository.Object, eventHub.Object,DocumentManager.Object);
            }
        }

        private string ProductId = "56d6954ca5fd61a1082ed788";

        private Product productModel
        {
            get
            {
                return new Product()
                {
                    Name = "Home Loan",
                    Description = "Loan for Home",
                    FamilyId = "1",
                    ProductCharacteristics = new ProductCharacteristics()
                    {
                        Accruals = new Interest()
                        {
                            InterestType =  InterestType.Variable,
                            CompoundingPeriod = FrequencyType.Annual,
                            TermDuration =  TermDuration.Month,
                            SupportedLoanTerms = new List<LoanTerm>()
                        {
                           new LoanTerm()
                           {
                               Frequency = FrequencyType.Annual,
                               Term =12
                           },
                           new LoanTerm()
                           {
                               Frequency = FrequencyType.Annual,
                               Term =36
                           }
                        },
                            PaymentFrequency = FrequencyType.Monthly,
                            PaymentType =  PaymentType.Installment,
                            PaymentTime =  PaymentTime.EndOfPeriod,
                            AmortizationCalendar =  AmortizationCalendar.Year360Days,
                            FirstPaymentDate = "10/10/2016",
                            SupportDeferredInterest = true,
                            DaysToStartInterestAccrual = "1"
                        },
                        Charters = new List<Charter>()
                    {
                        new Charter()
                        {
                            BankId = 1,
                            SupportedGeographies = new Geographies()
                            {
                                Country= new Country()
                                {
                                    Name= "USA",
                                    Code= "01"
                                },
                                ExcludedStates= new List<State>()
                                {
                                    new State()
                                    {
                                        CountryCode="01",
                                        Name="West Verginia",
                                        Code="WV"
                                    }
                                }
                            },
                        }
                    },
                        FundingSources = new List<FundingSource>()
                    {
                        new FundingSource()
                        {
                            Name="Test Funding Source"
                        }
                    },
                        PaymentHierarchies = new List<PaymentHierarchy>()
                    {
                        new PaymentHierarchy()
                        {
                            Priority=1,
                            PaymentElement= new PaymentElement()
                            {
                                Name="Test Element",
                                Description="Test Element Desc"
                            }
                        }
                    },
                        FeeCharges = new List<FeesCharge>()
                    {
                        new FeesCharge()
                        {
                            FeeType =  FeeType.LateFee,
                            FeeDescription = "Test Description",
                            FeeAmount = new Rule()
                            {
                                Name ="Test Rule",
                                Description ="Test Description"
                            },
                            Applicability = new Rule()
                            {
                                Name ="Test Rule",
                                Description ="Test Description"
                            },
                            ApplyInterest = true,
                            Amortize = true,
                            StartDate = tenantTime.Object.Now,
                            ExpiresOn = tenantTime.Object.Now,
                            ChartOfAccountMapping = 1
                        }
                    },
                        Documents = new List<ConsentDocument>()
                    {
                        new ConsentDocument()
                        {
                            ConsentPurpose = "Test",
                            DocumentDisplayName = "Test Document Name",
                            ValidFrom = tenantTime.Object.Now,
                            ExpiresOn = tenantTime.Object.Now,
                        }
                    },
                        UsaryRules = new List<UsuryRules>()
                    {
                        new UsuryRules()
                        {
                            RulePurpose = "ID Verification",
                            Rule = "Test Rule",
                            StartDate = tenantTime.Object.Now,
                            ExpiresOn = tenantTime.Object.Now,
                        }
                    }
                    }
                };
            }
        }

        private ProductRequestModel ModelToAdd
        {
            get
            {
                return new ProductRequestModel()
                {
                    Name = "Home Loan",
                    Description = "Loan for Home",
                    FamilyId = "1",
                    Accruals = new Interest()
                    {
                        InterestType = InterestType.Fixed,
                        CompoundingPeriod = FrequencyType.Annual,
                        TermDuration = TermDuration.Month,
                        SupportedLoanTerms = new List<LoanTerm>()
                        {
                           new LoanTerm()
                           {
                               Frequency= FrequencyType.Annual,
                               Term =12
                           },
                           new LoanTerm()
                           {
                               Frequency= FrequencyType.Annual,
                               Term =36
                           }
                        },
                        PaymentFrequency = FrequencyType.Annual,
                        PaymentType = PaymentType.OneTime,
                        PaymentTime = PaymentTime.BeginningOfPeriod,
                        AmortizationCalendar = AmortizationCalendar.Year360Days,
                        FirstPaymentDate = "10/10/2016",
                        SupportDeferredInterest = true,
                        DaysToStartInterestAccrual = "1"
                    },
                    Charters = new List<Charter>()
                    {
                        new Charter()
                        {
                            BankId = 1,
                            SupportedGeographies = new Geographies()
                            {
                                Country= new Country()
                                {
                                    Name= "USA",
                                    Code= "01"
                                },
                                ExcludedStates= new List<State>()
                                {
                                    new State()
                                    {
                                        CountryCode="01",
                                        Name="West Verginia",
                                        Code="WV"
                                    }
                                }
                            },
                        }
                    },
                    FundingSources = new List<FundingSource>()
                    {
                        new FundingSource()
                        {
                            Name="Test Funding Source"
                        }
                    },
                    PaymentHierarchies = new List<PaymentHierarchy>()
                    {
                        new PaymentHierarchy()
                        {
                            Priority=1,
                            PaymentElement= new PaymentElement()
                            {
                                Name="Test Element",
                                Description="Test Element Desc"
                            }
                        }
                    },
                    FeeCharges = new List<FeesCharge>()
                    {
                        new FeesCharge()
                        {
                            FeeType = FeeType.LateFee,
                            FeeDescription = "Test Description",
                            FeeAmount = new Rule()
                            {
                                Name ="Test Rule",
                                Description ="Test Description"
                            },
                            Applicability = new Rule()
                            {
                                Name ="Test Rule",
                                Description ="Test Description"
                            },
                            ApplyInterest = true,
                            Amortize = true,
                            StartDate = tenantTime.Object.Now,
                            ExpiresOn = tenantTime.Object.Now,
                            ChartOfAccountMapping = 1,
                             PaymentElement = new PaymentElement()
                            {
                                Name ="Test Rule",
                                Description ="Test Description"
                            },

                        }
                    },
                    Documents = new List<ConsentDocument>()
                    {
                        new ConsentDocument()
                        {
                            ConsentPurpose = "Test",
                            DocumentDisplayName = "Test Document Name",
                            ValidFrom = tenantTime.Object.Now,
                            ExpiresOn = tenantTime.Object.Now,
                             DocumentUrl ="testUrl"
                        }
                    },
                    UsaryRules = new List<UsuryRules>()
                    {
                        new UsuryRules()
                        {
                            RulePurpose = "ID Verification",
                            Rule = "Test Rile",
                            StartDate = tenantTime.Object.Now,
                            ExpiresOn = tenantTime.Object.Now,
                        }
                    }
                };
            }
        }

        #region Get Product

        [Fact]
        public async void Product_Get_Should_Return_Not_Null_On_Success()
        {
            productRepository.
                Setup(x => x.GetProductDetails(It.IsAny<string>())).
                ReturnsAsync(productModel);

            var result =await productService.Get(ProductId);

            productRepository.Verify(x => x.GetProductDetails(It.IsAny<string>()), Times.Once());

            Assert.NotNull(result);
        }

        [Fact]
        public  void Product_Get_Should_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() => productService.Get(string.Empty));
        }

        [Fact]
        public  void Product_Get_Should_Throw_Exception_On_Product_Not_Found()
        {
            productRepository.
                     Setup(x => x.GetProductDetails(It.IsAny<string>())).
                     ThrowsAsync(new NotFoundException("Not Found Exception"));

            Assert.ThrowsAsync<NotFoundException>(() => productService.Get(ProductId));
        }

        #endregion

        #region Add Product

        [Fact]
        public async void Product_Add_Should_Return_Product_Id_On_Success()
        {
            productRepository.
                Setup(x => x.Add(It.IsAny<Product>()));

            var result =await productService.Add(ModelToAdd);
            result = ProductId;

            productRepository.Verify(x => x.Add(It.IsAny<Product>()), Times.Once());

            Assert.NotEmpty(Convert.ToString(result));
        }

        [Fact]
        public  void Product_Add_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() =>productService.Add(null));
        }

        #endregion

        #region Update Product

        [Fact]
        public async void Product_Update_Should_Assert_True_On_Success()
        {
            productRepository.
                Setup(x => x.UpdateProduct(It.IsAny<string>(),It.IsAny<Product>())).
                ReturnsAsync(1);

            var testModel = ModelToAdd;           
            await productService.Update(ProductId,testModel);

            productRepository.Verify(x => x.UpdateProduct(It.IsAny<string>(),It.IsAny<Product>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public void Product_Update_Should_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => productService.Update(It.IsAny<string>(),ModelToAdd));
        }

        [Fact]
        public void Product_Update_Should_Throw_Exception_On_Product_Not_Found()
        {
            productRepository.
                Setup(x => x.UpdateProduct(It.IsAny<string>(), It.IsAny<Product>())).
                ReturnsAsync(0);
            Assert.ThrowsAsync<NotFoundException>(() => productService.Update(ProductId, ModelToAdd));
        }

        #endregion

        #region Delete Product

        [Fact]
        public async void Product_Delete_Should_Assert_True_On_Success()
        {
            productRepository.
                Setup(x => x.RemoveProduct(It.IsAny<string>())).
                ReturnsAsync(1);

           await productService.Delete(ProductId);

            productRepository.Verify(x => x.RemoveProduct(It.IsAny<string>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public void Product_Delete_Should_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() => productService.Delete(string.Empty));
        }

        [Fact]
        public void Product_Delete_Should_Throw_Exception_On_Product_Not_Found()
        {
            productRepository.
                Setup(x => x.RemoveProduct(It.IsAny<string>())).
                ReturnsAsync(0);
            Assert.ThrowsAsync<NotFoundException>(() =>productService.Delete(ProductId));
        }

        #endregion

        #region Activate Product

        [Fact]
        public async void Product_Activate_Should_Assert_True_On_Success()
        {
            productRepository.
                Setup(x => x.ActivateProduct(It.IsAny<string>())).
                ReturnsAsync(1);

            await productService.Activate(ProductId);

            productRepository.Verify(x => x.ActivateProduct(It.IsAny<string>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public void Product_Activate_Should_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() =>productService.Activate(string.Empty));
        }

        [Fact]
        public void Product_Activate_Should_Throw_Exception_On_Product_Not_Found()
        {
            productRepository.
              Setup(x => x.ActivateProduct(It.IsAny<string>())).
              ReturnsAsync(0);
            Assert.ThrowsAsync<NotFoundException>(() =>productService.Activate(ProductId));
        }

        #endregion

        #region Expire Product

        [Fact]
        public async void Product_Expire_Should_Assert_True_On_Success()
        {
            productRepository.
                Setup(x => x.DeActivateProduct(It.IsAny<string>())).
                ReturnsAsync(1);

           await productService.Expire(ProductId);

            productRepository.Verify(x => x.DeActivateProduct(It.IsAny<string>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public void Product_Expire_Should_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() => productService.Expire(string.Empty));
        }

        [Fact]
        public void Product_Expire_Should_Throw_Exception_On_Product_Not_Found()
        {
            productRepository.
              Setup(x => x.DeActivateProduct(It.IsAny<string>())).
              ReturnsAsync(0);
            Assert.ThrowsAsync<NotFoundException>(() =>productService.Expire(ProductId));
        }

        #endregion

        #region Add Interest

        [Fact]
        public async void Interest_Add_Should_Assert_True_On_Success()
        {
            //productRepository.
            //    Setup(x => x.AddInterest(It.IsAny<string>(), It.IsAny<IInterest>()));

            await productService.AddInterest(ProductId, ModelToAdd.Accruals);

            productRepository.Verify(x => x.AddInterest(It.IsAny<string>(), It.IsAny<IInterest>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public  void Interest_Add_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() =>productService.AddInterest(string.Empty, null));
        }

        #endregion

        #region Update Interest

        [Fact]
        public async void Interest_Update_Should_Assert_True_On_Success()
        {
            productRepository.
                Setup(x => x.UpdateInterest(It.IsAny<string>(), It.IsAny<IInterest>())).
                ReturnsAsync(1);

            await productService.UpdateInterest(ProductId, ModelToAdd.Accruals);

            productRepository.Verify(x => x.UpdateInterest(It.IsAny<string>(), It.IsAny<IInterest>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public void Interest_Update_Should_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() => productService.UpdateInterest(string.Empty, null));
        }

        [Fact]
        public void Interest_Update_Should_Throw_Exception_On_Product_Not_Found()
        {
            productRepository.
                 Setup(x => x.UpdateInterest(It.IsAny<string>(), It.IsAny<IInterest>())).
                ReturnsAsync(0);
            Assert.ThrowsAsync<NotFoundException>(() =>productService.UpdateInterest(ProductId, ModelToAdd.Accruals));
        }

        #endregion

        #region Add Charter

        [Fact]
        public async void Charter_Add_Should_Assert_True_On_Success()
        {
            productRepository.
                Setup(x => x.AddCharter(It.IsAny<string>(), It.IsAny<List<ICharter>>())).
                ReturnsAsync(1);

            await productService.AddCharter(ProductId, ModelToAdd.Charters.ToList<ICharter>());

            productRepository.Verify(x => x.AddCharter(It.IsAny<string>(), It.IsAny<List<ICharter>>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public void Charter_Add_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() =>productService.AddCharter(string.Empty, null));
        }

        #endregion

        #region Update Charter

        [Fact]
        public async void Charter_Update_Should_Assert_True_On_Success()
        {
            productRepository.
                Setup(x => x.UpdateCharter(It.IsAny<string>(), It.IsAny<List<ICharter>>())).
                ReturnsAsync(1);

            await productService.UpdateCharter(ProductId, ModelToAdd.Charters.ToList<ICharter>());

            productRepository.Verify(x => x.UpdateCharter(It.IsAny<string>(), It.IsAny<List<ICharter>>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public void Charter_Update_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() =>productService.UpdateCharter(string.Empty, null));
        }

        [Fact]
        public void Charter_Update_Should_Throw_Exception_On_Product_Not_Found()
        {
            productRepository.
                Setup(x => x.UpdateCharter(It.IsAny<string>(), It.IsAny<List<ICharter>>())).
              ReturnsAsync(0);
            Assert.ThrowsAsync<NotFoundException>(() => productService.UpdateCharter(ProductId, ModelToAdd.Charters.ToList<ICharter>()));
        }

        #endregion

        #region Add Funding Source

        [Fact]
        public async void FundingSource_Add_Should_Assert_True_On_Success()
        {
            productRepository.
                Setup(x => x.AddFundingSource(It.IsAny<string>(), It.IsAny<List<IFundingSource>>())).
                ReturnsAsync(1);

            await productService.AddFundingSource(ProductId, ModelToAdd.FundingSources.ToList<IFundingSource>());

            productRepository.Verify(x => x.AddFundingSource(It.IsAny<string>(), It.IsAny<List<IFundingSource>>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public void FundingSource_Add_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() =>productService.AddFundingSource(string.Empty, null));
        }

        #endregion

        #region Update Funding Source

        [Fact]
        public async void FundingSource_Update_Should_Assert_True_On_Success()
        {
            productRepository.
                Setup(x => x.UpdateFundingSource(It.IsAny<string>(), It.IsAny<List<IFundingSource>>())).
                ReturnsAsync(1);

            await productService.UpdateFundingSource(ProductId, ModelToAdd.FundingSources.ToList<IFundingSource>());

            productRepository.Verify(x => x.UpdateFundingSource(It.IsAny<string>(), It.IsAny<List<IFundingSource>>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public  void FundingSource_Update_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() =>productService.UpdateFundingSource(string.Empty, null));
        }

        [Fact]
        public void FundingSource_Update_Should_Throw_Exception_On_Product_Not_Found()
        {
            productRepository.
                     Setup(x => x.UpdateFundingSource(It.IsAny<string>(), It.IsAny<List<IFundingSource>>())).
                   ReturnsAsync(0);

            Assert.ThrowsAsync<NotFoundException>(() =>productService.UpdateFundingSource(ProductId, ModelToAdd.FundingSources.ToList<IFundingSource>()));
        }

        #endregion

        #region Add Payment Hierarchies

        [Fact]
        public async void PaymentHierarchies_Add_Should_Assert_True_On_Success()
        {
            productRepository.
                Setup(x => x.AddPaymentHierarchies(It.IsAny<string>(), It.IsAny<List<IPaymentHierarchy>>())).
                ReturnsAsync(1);

            await productService.AddPaymentHierarchies(ProductId, ModelToAdd.PaymentHierarchies.ToList<IPaymentHierarchy>());

            productRepository.Verify(x => x.AddPaymentHierarchies(It.IsAny<string>(), It.IsAny<List<IPaymentHierarchy>>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public void PaymentHierarchies_Add_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() =>productService.AddPaymentHierarchies(string.Empty, null));
        }

        #endregion

        #region Update Payment Hierarchies

        [Fact]
        public async void PaymentHierarchies_Update_Should_Assert_True_On_Success()
        {
            productRepository.
                Setup(x => x.UpdatePaymentHierarchies(It.IsAny<string>(), It.IsAny<List<IPaymentHierarchy>>())).
                ReturnsAsync(1);

            await productService.UpdatePaymentHierarchies(ProductId, ModelToAdd.PaymentHierarchies.ToList<IPaymentHierarchy>());

            productRepository.Verify(x => x.UpdatePaymentHierarchies(It.IsAny<string>(), It.IsAny<List<IPaymentHierarchy>>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public void PaymentHierarchies_Update_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() => productService.UpdatePaymentHierarchies(string.Empty, null));
        }

        [Fact]
        public void PaymentHierarchies_Update_Should_Throw_Exception_On_Product_Not_Found()
        {
            productRepository.
                 Setup(x => x.UpdatePaymentHierarchies(It.IsAny<string>(), It.IsAny<List<IPaymentHierarchy>>())).
               ReturnsAsync(0);
            Assert.ThrowsAsync<NotFoundException>(() =>productService.UpdatePaymentHierarchies(ProductId, ModelToAdd.PaymentHierarchies.ToList<IPaymentHierarchy>()));
        }

        #endregion

        #region Add Fees Charge

        [Fact]
        public async void FeesCharge_Add_Should_Assert_True_On_Success()
        {
            productRepository.
                Setup(x => x.AddFeeCharge(It.IsAny<string>(), It.IsAny<List<IFeesCharge>>())).
                ReturnsAsync(1);

            await productService.AddFeesCharge(ProductId, ModelToAdd.FeeCharges.ToList<IFeesCharge>());

            productRepository.Verify(x => x.AddFeeCharge(It.IsAny<string>(), It.IsAny<List<IFeesCharge>>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public void FeesCharge_Add_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() => productService.AddFeesCharge(string.Empty, null));
        }

        #endregion

        #region Update Fees Charge

        [Fact]
        public async void FeesCharge_Update_Should_Assert_True_On_Success()
        {
            productRepository.
                Setup(x => x.UpdateFeeCharge(It.IsAny<string>(), It.IsAny<List<IFeesCharge>>())).
                ReturnsAsync( 1);

            await productService.UpdateFeesCharge(ProductId, ModelToAdd.FeeCharges.ToList<IFeesCharge>());

            productRepository.Verify(x => x.UpdateFeeCharge(It.IsAny<string>(), It.IsAny<List<IFeesCharge>>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public void FeesCharge_Update_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() =>productService.UpdateFeesCharge(string.Empty, null));
        }

        [Fact]
        public void FeesCharge_Update_Should_Throw_Exception_On_Product_Not_Found()
        {
            productRepository.
                 Setup(x => x.UpdateFeeCharge(It.IsAny<string>(), It.IsAny<List<IFeesCharge>>())).
               ReturnsAsync(0); 
            Assert.ThrowsAsync<NotFoundException>(() => productService.UpdateFeesCharge(ProductId, ModelToAdd.FeeCharges.ToList<IFeesCharge>()));
        }

        #endregion

        #region Add Consent Document

        [Fact]
        public async void ConsentDocument_Add_Should_Assert_True_On_Success()
        {
            productRepository.
                Setup(x => x.AddConsentDocument(It.IsAny<string>(), It.IsAny<List<IConsentDocument>>())).
                ReturnsAsync(1);

            await productService.AddConsentDocument(ProductId, ModelToAdd.Documents.ToList<IConsentDocument>());

            productRepository.Verify(x => x.AddConsentDocument(It.IsAny<string>(), It.IsAny<List<IConsentDocument>>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public void ConsentDocument_Add_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() =>productService.AddConsentDocument(string.Empty, null));
        }

        #endregion

        #region Update Consent Document

        [Fact]
        public async void ConsentDocument_Update_Should_Assert_True_On_Success()
        {
            productRepository.
                Setup(x => x.UpdateConsentDocument(It.IsAny<string>(), It.IsAny<List<IConsentDocument>>())).
                ReturnsAsync(1);

           await productService.UpdateConsentDocument(ProductId, ModelToAdd.Documents.ToList<IConsentDocument>());

            productRepository.Verify(x => x.UpdateConsentDocument(It.IsAny<string>(), It.IsAny<List<IConsentDocument>>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public void ConsentDocument_Update_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() =>productService.UpdateConsentDocument(string.Empty, null));
        }

        [Fact]
        public void ConsentDocument_Update_Should_Throw_Exception_On_Product_Not_Found()
        {

            productRepository.
              Setup(x => x.UpdateConsentDocument(It.IsAny<string>(), It.IsAny<List<IConsentDocument>>())).
            ReturnsAsync(0);

            Assert.ThrowsAsync<NotFoundException>(() =>productService.UpdateConsentDocument(ProductId, ModelToAdd.Documents.ToList<IConsentDocument>()));
        }

        #endregion

        #region Add Usury Rules

        [Fact]
        public async void UsuryRules_Add_Should_Assert_True_On_Success()
        {
            productRepository.
                Setup(x => x.AddUsuryRules(It.IsAny<string>(), It.IsAny<List<IUsuryRules>>())).
                ReturnsAsync(1);

            await productService.AddUsuryRule(ProductId, ModelToAdd.UsaryRules.ToList<IUsuryRules>());

            productRepository.Verify(x => x.AddUsuryRules(It.IsAny<string>(), It.IsAny<List<IUsuryRules>>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public void UsuryRules_Add_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() =>productService.AddUsuryRule(string.Empty, null));
        }

        #endregion

        #region Update Usury Rules

        [Fact]
        public async void UsuryRules_Update_Should_Assert_True_On_Success()
        {
            productRepository.
                Setup(x => x.UpdateUsuryRules(It.IsAny<string>(), It.IsAny<List<IUsuryRules>>())).
                ReturnsAsync(1);

           await productService.UpdateUsuryRule(ProductId, ModelToAdd.UsaryRules.ToList<IUsuryRules>());

            productRepository.Verify(x => x.UpdateUsuryRules(It.IsAny<string>(), It.IsAny<List<IUsuryRules>>()), Times.Once());

            Assert.True(true);
        }

        [Fact]
        public void UsuryRules_Update_Throw_Exception_On_Validation_Fail()
        {
            Assert.ThrowsAsync<ArgumentException>(() =>productService.UpdateUsuryRule(string.Empty, null));
        }

        [Fact]
        public void UsuryRules_Update_Should_Throw_Exception_On_Product_Not_Found()
        {
            productRepository.
                Setup(x => x.UpdateUsuryRules(It.IsAny<string>(), It.IsAny<List<IUsuryRules>>())).
              ReturnsAsync(0);
            Assert.ThrowsAsync<NotFoundException>(() =>productService.UpdateUsuryRule(ProductId, ModelToAdd.UsaryRules.ToList<IUsuryRules>()));
        }

        #endregion
    }
}
